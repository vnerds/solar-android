package com.vnerds.solarpanel.objects;

import com.google.gson.annotations.SerializedName;

/**
 * Created by parag on 8/22/2017.
 */

public class CommonResponseObject {


    public String getSuccess_msg() {
        return success_msg;
    }

    public void setSuccess_msg(String success_msg) {
        this.success_msg = success_msg;
    }

    /**
     * error : 1
     * error_msg : Login credentials are wrong. Please try again!
     */

    private int error;
    private String error_msg,success_msg;

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }
}
