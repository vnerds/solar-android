package com.vnerds.solarpanel.apis;


import com.vnerds.solarpanel.objects.ChannelListObject;
import com.vnerds.solarpanel.objects.CountryCode;
import com.vnerds.solarpanel.objects.DepartmentListObject;
import com.vnerds.solarpanel.objects.DeviceListObject;
import com.vnerds.solarpanel.objects.GraphObject;
import com.vnerds.solarpanel.objects.GraphObjectWithoutDay;
import com.vnerds.solarpanel.objects.LoginObject;
import com.vnerds.solarpanel.objects.OEMListObject;
import com.vnerds.solarpanel.objects.PlantListObject;
import com.vnerds.solarpanel.objects.StateListObject;
import com.vnerds.solarpanel.objects.WorkingInfoObject;
import com.vnerds.solarpanel.objects.WorkingInfo_InvInfo;
import com.vnerds.solarpanel.objects.YearListObject;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIServices {
//http://117.247.80.164:85/herapi/plant_view.php?
// tag=device_list&
// plant_id=340&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjciLCJlbWFpbCI6IkhUUEwiLCJleHAiOjE1NDM3MzY0MTd9.CMdgF3LToNUdc31paWcNaMlqOxYsLNyPsb3Zbj8NZS0

  @GET(APIClass.plantList)
  Call<PlantListObject> getPlantList(
          @Query(APIClass.PAGE_NUMBER) int pageNumber,
          @Query(APIClass.LIMIT) int limit,
          @Query(APIClass.OEM_ID) String oem_id,
          @Query(APIClass.STATE_ID) String state_id,
          @Query(APIClass.DEPARTMENT_ID) String DEPARTMENT_ID,
          @Query(APIClass.YEAR) int year,
          @Query(APIClass.TAG) String tag

  );
  @GET(APIClass.plantView)
  Call<DeviceListObject> getDeviceList(
          @Query(APIClass.PLANT_ID) String plant_id,
          @Query(APIClass.TOKEN) String token,
          @Query(APIClass.TAG) String tag

  );
  @GET(APIClass.plantDevice)
  Call<DeviceListObject> getDeviceGraphList(
          @Query(APIClass.ID) String plant_id,
          @Query(APIClass.TOKEN) String token,
          @Query(APIClass.TAG) String tag

  );

  @GET(APIClass.plantView)
  Call<WorkingInfoObject> getWorkingInfo(
          @Query(APIClass.PLANT_ID) String plant_id,
          @Query(APIClass.TOKEN) String token,
          @Query(APIClass.TAG) String tag

  );
  ////http://117.247.80.164:85/herapi/plant_inv_graph.php?tag=graph&id=6985&key=day&val=15-05-2019&chart_type=Output_Power&token=ey
  @GET(APIClass.plantInvGraph)
  Call<GraphObject> getPlantInvGraph(
          @Query(APIClass.ID) String id,
          @Query(APIClass.KEY) String key,
          @Query(APIClass.VAL) String val,
          @Query(APIClass.CHART_TYPE) String chart_type,
          @Query(APIClass.TOKEN) String token,
          @Query(APIClass.TAG) String tag

  ); ////http://117.247.80.164:85/herapi/plant_inv_graph.php?tag=graph&id=6985&key=day&val=15-05-2019&chart_type=Output_Power&token=ey
  @GET(APIClass.plantInvGraph)
  Call<GraphObjectWithoutDay> getPlantInvGraphWithoutDay(
          @Query(APIClass.ID) String id,
          @Query(APIClass.KEY) String key,
          @Query(APIClass.VAL) String val,
          @Query(APIClass.CHART_TYPE) String chart_type,
          @Query(APIClass.TOKEN) String token,
          @Query(APIClass.TAG) String tag
  );
  @GET(APIClass.plantDevice)
  Call<WorkingInfoObject> getWorkingGraphInfo(
          @Query(APIClass.ID) String plant_id,
          @Query(APIClass.TOKEN) String token,
          @Query(APIClass.TAG) String tag

  );
  @GET(APIClass.plantView)
  Call<WorkingInfo_InvInfo> getWorkingInvInfo(
          @Query(APIClass.PLANT_ID) String plant_id,
          @Query(APIClass.TOKEN) String token,
          @Query(APIClass.TAG) String tag

  );
  @GET(APIClass.plantDevice)
  Call<WorkingInfo_InvInfo> getWorkingInvGraphInfo(
          @Query(APIClass.ID) String plant_id,
          @Query(APIClass.TOKEN) String token,
          @Query(APIClass.TAG) String tag

  );
  //http://www.wcst.co.in/herapi/oem.php?
  // tag=list&
  // page_num=1&
  // limit=4&
  // filter_oem=de,
  // filter_plant,
  // token

  @GET(APIClass.oemList)
  Call<PlantListObject> getSearchPlantList(
          @Query(APIClass.TAG) String tag,
          @Query(APIClass.PAGE_NUMBER) int pageNumber,
          @Query(APIClass.LIMIT) int limit,
          @Query(APIClass.FILTER_PLANT) String FILTER_PLANT,
          @Query(APIClass.TOKEN) String token
  );

  @GET(APIClass.plantList)
  Call<PlantListObject> editPlantList(
          @Query(APIClass.PAGE_NUMBER) int pageNumber,
          @Query(APIClass.LIMIT) int limit,
          @Query(APIClass.OEM_ID) String oem_id,
          @Query(APIClass.STATE_ID) String state_id,
          @Query(APIClass.DEPARTMENT_ID) String DEPARTMENT_ID,
          @Query(APIClass.YEAR) int year,
          @Query(APIClass.TAG) String tag

  );

  @GET(APIClass.departmentList)
  Call<DepartmentListObject> getDepartmentList(
          @Query(APIClass.PAGE_NUMBER) int pageNumber,
          @Query(APIClass.LIMIT) int limit,
          @Query(APIClass.TOKEN) String token,
          @Query(APIClass.TAG) String tag,
          @Query(APIClass.OEM_ID) String oem_id,
          @Query(APIClass.STATE_ID) String state_id
  );
  @GET(APIClass.oemList)
  Call<DepartmentListObject> getDepartmentSearchList(
          @Query(APIClass.PAGE_NUMBER) int pageNumber,
          @Query(APIClass.LIMIT) int limit,
          @Query(APIClass.TOKEN) String token,
          @Query(APIClass.TAG) String tag,
          @Query(APIClass.FILTER_OEM) String FILTER_OEM, @Query(APIClass.OEM_ID) String oem_id,
          @Query(APIClass.STATE_ID) String state_id
  );

  @GET(APIClass.departmentList)
  Call<DepartmentListObject> deleteDepartment(
          @Query(APIClass.TAG) String tag,
          @Query(APIClass.DEPARTMENT_ID) String oemId

  );

  @GET(APIClass.oemList)
  Call<OEMListObject> getOEMList(
          @Query(APIClass.PAGE_NUMBER) int pageNumber,
          @Query(APIClass.LIMIT) int limit,
          @Query(APIClass.TOKEN) String token,
          @Query(APIClass.TAG) String tag
  );
  @GET(APIClass.stateList)
  Call<StateListObject> getStateList(
          @Query(APIClass.PAGE_NUMBER) int pageNumber,
          @Query(APIClass.LIMIT) int limit,
          @Query(APIClass.TOKEN) String token,
          @Query(APIClass.TAG) String tag,
          @Query(APIClass.OEM_ID) String oemId
  );

  @GET(APIClass.yearList)
  Call<YearListObject> getYearList(
          @Query(APIClass.OEM_ID) String OEM_ID,
          @Query(APIClass.STATE_ID) String STATE_ID,
          @Query(APIClass.DEPARTMENT_ID) String DEPARTMENT_ID,

          @Query(APIClass.TAG) String tag

  );
  @GET(APIClass.oemList)
  Call<OEMListObject> getOEMSearchList(
          @Query(APIClass.PAGE_NUMBER) int pageNumber,
          @Query(APIClass.LIMIT) int limit,
          @Query(APIClass.TOKEN) String token,
          @Query(APIClass.TAG) String tag,
          @Query(APIClass.FILTER_OEM) String FILTER_OEM
  );


    @POST(APIClass.login)
    @FormUrlEncoded
    Call<LoginObject> login(
            @Field(APIClass.USERNAME) String username,
            @Field(APIClass.PASS) String password,
            @Field(APIClass.TOKEN) String token
    );

  @POST(APIClass.oemList)
  @FormUrlEncoded
  Call<LoginObject> addOEM(
          @Field(APIClass.NAME) String name,
          @Field(APIClass.ADDRESS) String address,
          @Field(APIClass.USERNAME) String username,
          @Field(APIClass.PASSWORD) String password,
          @Field(APIClass.TOKEN) String token,
          @Query(APIClass.TAG) String tag
  );
  @POST(APIClass.stateList)
  @FormUrlEncoded
  Call<LoginObject> addState(
          @Field(APIClass.NAME) String name,
          @Field(APIClass.OEM_ID) String oemId,
          @Field(APIClass.TOKEN) String token,
          @Query(APIClass.TAG) String tag
  );
  @POST(APIClass.oemList)
  @FormUrlEncoded
  Call<LoginObject> editOEM(
          @Field(APIClass.OEM_ID) String oemID,
          @Field(APIClass.NAME) String name,
          @Field(APIClass.ADDRESS) String address,
          @Field(APIClass.USERNAME) String username,
          @Field(APIClass.PASSWORD) String password,
          @Field(APIClass.TOKEN) String token,
          @Query(APIClass.TAG) String tag
  );
 @POST(APIClass.stateList)
  @FormUrlEncoded
  Call<LoginObject> editState(
          @Field(APIClass.OEM_ID) String oemID,
          @Field(APIClass.STATE_ID) String stateID,
          @Field(APIClass.NAME) String name,
          @Field(APIClass.TOKEN) String token,
          @Query(APIClass.TAG) String tag
  );

  //name, oem_id, address, state_id, uname and password, token
    @POST(APIClass.departmentList)
    @FormUrlEncoded
    Call<LoginObject> addDepartment(
            @Field(APIClass.NAME) String name,
            @Field(APIClass.OEM_ID) String oemId,
            @Field(APIClass.ADDRESS) String address,
            @Field(APIClass.STATE_ID) String stateId,
            @Field(APIClass.USERNAME) String username,
            @Field(APIClass.PASSWORD) String password,
            @Field(APIClass.TOKEN) String token,
            @Query(APIClass.TAG) String tag
            ,
            @Field(APIClass.CITY) String city
    );
    @POST(APIClass.departmentList)
    @FormUrlEncoded
    Call<LoginObject> editDepartment(
            @Field(APIClass.DEPARTMENT_ID) String department_id,
            @Field(APIClass.NAME) String name,
            @Field(APIClass.OEM_ID) String oemId,
            @Field(APIClass.ADDRESS) String address,
            @Field(APIClass.STATE_ID) String stateId,
            @Field(APIClass.USERNAME) String username,
            @Field(APIClass.PASSWORD) String password,
            @Field(APIClass.TOKEN) String token,
            @Query(APIClass.TAG) String tag,
            @Field(APIClass.CITY) String city
    );
  @GET(APIClass.oemList)
  Call<OEMListObject> deleteOEM(
          @Query(APIClass.TAG) String tag,
          @Query(APIClass.OEM_ID) String  oemId,
             @Query(APIClass.TOKEN) String token

  );
  @GET(APIClass.stateList)
  Call<StateListObject> deleteState(
          @Query(APIClass.TAG) String tag,
          @Query(APIClass.STATE_ID) String  state_id,
             @Query(APIClass.TOKEN) String token

  );


}
