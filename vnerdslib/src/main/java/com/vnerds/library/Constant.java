package com.vnerds.library;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by parag on 8/22/2017.
 */

public class Constant {
    public static boolean isOEMAdded = true;


    public static Retrofit getRetrofitClient(String baseURL, boolean isLogUnable) {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);


        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        // httpClient.readTimeout(60000, TimeUnit.SECONDS);
        httpClient.readTimeout(60, TimeUnit.MINUTES);
        httpClient.connectTimeout(60, TimeUnit.MINUTES);
        httpClient.writeTimeout(60, TimeUnit.MINUTES);

        Retrofit.Builder retrofitBuilder = new Retrofit.Builder();
        retrofitBuilder.baseUrl(baseURL);
        if (isLogUnable) {
            retrofitBuilder.client(httpClient.build());//For unable log
        }
        retrofitBuilder.addConverterFactory(GsonConverterFactory.create(gson));

        Retrofit retrofit = retrofitBuilder.build();

        return retrofit;
    }
    public static void callBrowser(Context context ,String url){
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(browserIntent);

    }
    public static String getCountryDialCode(Context context, String contryId){

        String contryDialCode = null;
        contryId = contryId.toUpperCase();
        String[] arrContryCode=context.getResources().getStringArray(R.array.DialingCountryCode);
        for(int i=0; i<arrContryCode.length; i++){
            String[] arrDial = arrContryCode[i].split(",");
            if(arrDial[1].trim().equals(contryId.trim())){
                contryDialCode = arrDial[0];
                break;
            }
        }
        return contryDialCode;
    }
    public static String getCurrentDate(String formatter){
        //yyyy-MM-dd
        DateFormat dateFormat = new SimpleDateFormat(formatter);
        Date date = new Date();
        return (dateFormat.format(date));
    }
    public static void changeStatusBarColor(AppCompatActivity context, int color){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = context.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(color);
        }
    }

    public static void showKeyboard(Context context) {
        InputMethodManager imm = (InputMethodManager)
                context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
        }
    }


    public static int dpToPx(Context context,int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public int pxToDp(Context context,int px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }


    public static void hideKeyboard(Activity activity) {
        /*InputMethodManager imm = (InputMethodManager)
                context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
        }*/
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    public static void restartApp(Activity activity,Class className){
        Intent intent = new Intent(activity,className);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
        activity.finish();
    }

    public static void noTransitionCallingActivity(Activity activity) {
        activity.overridePendingTransition(0, 0);
    }
    public static void callActivity(Activity activity, Class className, Bundle bundle,boolean wantToFinish) {
        Intent intent = new Intent(activity, className);
        if(bundle!=null) {
            intent.putExtras(bundle);
        }
        activity.startActivity(intent);
        if(wantToFinish){
            activity.finish();
        }
    } 
   

    public static boolean isGPSEnable(Context context) {

        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }


    }
    public static byte[] urlToByteArray(String url) {
        Bitmap bitmap = null;
        InputStream in = null;
        BufferedOutputStream out = null;
        int IO_BUFFER_SIZE = 4 * 1024;
        try {
            URI uri = new URI(url);
            url = uri.toASCIIString();
            in = new BufferedInputStream(new URL(url).openStream(),
                    IO_BUFFER_SIZE);
            final ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
            out = new BufferedOutputStream(dataStream, IO_BUFFER_SIZE);
            int bytesRead;
            byte[] buffer = new byte[IO_BUFFER_SIZE];
            while ((bytesRead = in.read(buffer)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
            out.flush();
            final byte[] data = dataStream.toByteArray();
            return data;

        } catch (IOException e) {
            return null;
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;


    }

    public boolean isLocationPermissionEnabled(Context context) {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }
    public static String getAndroidSecureID(Context context){
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static float getDistanceInKM(double sLat,double sLon ,double dLat,double dLon){
        Location startPoint=new Location("locationA");
        startPoint.setLatitude(sLat);
        startPoint.setLongitude(sLon);

        Location endPoint=new Location("locationB");
        endPoint.setLatitude(dLat);
        endPoint.setLongitude(dLon);

        float distance=startPoint.distanceTo(endPoint)/1000;
        // DecimalFormat form = new DecimalFormat("0.00");
        return (distance);
    }

    public static void openMessageAlert(Activity activity,String title,String msg,String btnText) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(btnText, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
               ;
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();

            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    Log.i("Class", info[i].getState().toString());
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static String getDifferenceBetweenDateWithUTC(String dateAndtime,String formatter){
        String dHMY ="";
        String dateStart = dateAndtime;
        //For an example yyyy-MM-dd HH:mm:ss

        SimpleDateFormat format = new SimpleDateFormat(formatter,Locale.US);

        Date saved_date = null;
        Date c_date = null;

        try {
            saved_date = format.parse(dateStart);
            Calendar currentTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            currentTime.set(Calendar.ZONE_OFFSET, TimeZone.getTimeZone("UTC").getRawOffset());
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, currentTime.get(Calendar.HOUR_OF_DAY));
            calendar.set(Calendar.MINUTE, currentTime.get(Calendar.MINUTE));
            calendar.set(Calendar.SECOND, currentTime.get(Calendar.SECOND));

            c_date =calendar.getTime();

            long diffMs = c_date.getTime() - saved_date.getTime();
            long diffSec = diffMs / 1000;
            long min = diffSec / 60;
            long sec = diffSec % 60;
            long hour = diffSec/3600;
            dHMY = ""+hour;
            if(hour>24){
                int day = (int)hour/24;
                if(day>30){
                    int month = day/30;
                    if(month>11){
                        int year = month/12;
                        dHMY = year +"y" ;
                    }else{
                        dHMY = month +"m" ;
                    }
                }else{
                    dHMY = day +"d" ;
                }
            }else{
                if(hour > 0){
                    dHMY = hour+"h";
                }else if(min>0){
                    dHMY = min+"m";
                }else{
                    dHMY = sec+"s";
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return dHMY;
    }

    public static void playVideo(Context context ,String newVideoPath){

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(newVideoPath));
        intent.setDataAndType(Uri.parse(newVideoPath), "video/*");
        context.startActivity(intent);

    }
    public static void callMail(Context context ,String email,String subject,String body){
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + email));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, body);
       //emailIntent.putExtra(Intent.EXTRA_HTML_TEXT, body); //If you are using HTML in your body text
        context.startActivity((emailIntent));
    }
    public static String[] getCountryAndAreaName(Context context, double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(context, Locale.ENGLISH);
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            Address result;

            if (addresses != null && !addresses.isEmpty()) {
                String[] arr = new String[4];
                if(addresses.get(0).getLocality()!= null && addresses.get(0).getLocality().length()>0) {
                    arr[0] = addresses.get(0).getLocality();
                }else{
                    arr[0] = "";
                }
                if(addresses.get(0).getAdminArea()!= null && addresses.get(0).getAdminArea().length()>0) {
                    arr[1] = addresses.get(0).getAdminArea();
                }else{
                    arr[1] = "";
                }
                if(addresses.get(0).getCountryCode()!= null && addresses.get(0).getCountryCode().length()>0) {
                    arr[2] = addresses.get(0).getCountryCode();
                }else{
                    arr[2] = "";
                }
                return arr;
            }

        } catch (Exception ignored) {
            //do something
        }
        return null;
    }
    public static String getDoubleValueToString(double value){
        return String.format("%.2f",value);
    }
}
