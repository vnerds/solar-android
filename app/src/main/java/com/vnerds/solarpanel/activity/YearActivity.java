package com.vnerds.solarpanel.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vnerds.custom.SnEditText;
import com.vnerds.custom.SnTextView;
import com.vnerds.library.Constant;
import com.vnerds.solarpanel.R;
import com.vnerds.solarpanel.apis.APIClass;
import com.vnerds.solarpanel.application.SolarPanelApplication;
import com.vnerds.solarpanel.objects.Global;
import com.vnerds.solarpanel.objects.StateListObject;
import com.vnerds.solarpanel.objects.YearListObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class YearActivity extends BaseActivity {

    @BindView(R.id.etSearchText)
    SnEditText etSearchText;

    @OnClick(R.id.btnSearch)
    public void onClickSearch(View view) {
        etSearchText.setText("");
        myRecyclerViewAdapter.update(oemBeanListMain);
        Constant.hideKeyboard(this);
    }

    @BindView(R.id.rvHome)
    RecyclerView rvHome;

    @BindView(R.id.linSearch)
    LinearLayout linSearch;

    MyRecyclerViewAdapter myRecyclerViewAdapter;

    LinearLayoutManager llm;
    List<YearListObject.YearBean> oemBeanListMain;
    Activity mActivity;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oem);

        mActivity = this;
        ButterKnife.bind(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.show();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(getString(R.string.year));
        linSearch.setVisibility(View.GONE);


        oemBeanListMain = new ArrayList<>();
        myRecyclerViewAdapter = new MyRecyclerViewAdapter(oemBeanListMain);

        rvHome.setAdapter(myRecyclerViewAdapter);
        rvHome.setNestedScrollingEnabled(false);
        llm = new LinearLayoutManager(this);
        rvHome.setLayoutManager(new GridLayoutManager(getApplicationContext(),2));


        getYearList();


    }

    @Override
    protected void onResume() {
        super.onResume();

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                Constant.callActivity(this, AddStateActivity.class, null, false);
                break;
            case R.id.action_logout:
                logOutAlert();
                break;
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }

        return true;
    }



    public void callSearchOEMAPI() {
        if (isNetworkAvailable()) {


        }
    }

    public void getYearList() {
        if (isNetworkAvailable()) {
            BaseActivity.showDialog();
            Call<YearListObject> call = SolarPanelApplication.getApiServices().getYearList(  Global.oemBean.getOem_id(), Global.stateBean.getState_id(), Global.departmentBean.getDepartment_id(), APIClass.LIST);
            call.enqueue(new Callback<YearListObject>() {
                @Override
                public void onResponse(Call<YearListObject> call, Response<YearListObject> response) {
                    BaseActivity.hideDialog();
                    final YearListObject StateListObject = response.body();
                    if (isValidResponse(StateListObject)) {
                        oemBeanListMain = StateListObject.getYear();
                        myRecyclerViewAdapter.update(oemBeanListMain);
                    } else if (response.errorBody() != null) {
                        try {
                            showErrorMsg(response.errorBody().string(), response.code());
                        } catch (Exception e) {

                        }
                    }

                }

                @Override
                public void onFailure(Call<YearListObject> call, Throwable t) {
                    hideDialog();
                    showToast(t.getMessage());
                }
            });

        }
    }




    public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.DataObjectHolder> {

        private List<YearListObject.YearBean> mDataset;


        public class DataObjectHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.tvYear)
            SnTextView tvYear;



            public DataObjectHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

        }


        public List<YearListObject.YearBean> getListView() {
            return mDataset;
        }

        public YearListObject.YearBean getSelectedItem(int position) {
            return mDataset.get(position);
        }

        public MyRecyclerViewAdapter(List<YearListObject.YearBean> myDataset) {
            mDataset = myDataset;
        }

        public void update(List<YearListObject.YearBean> myDataset) {
            mDataset = myDataset;
            notifyDataSetChanged();
        }

        @Override
        public MyRecyclerViewAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                         int viewType) {
            View view;
            {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.activity_year_list_item, parent, false);
            }

            MyRecyclerViewAdapter.DataObjectHolder dataObjectHolder = new MyRecyclerViewAdapter.DataObjectHolder(view);
            return dataObjectHolder;
        }

        @Override
        public void onBindViewHolder(MyRecyclerViewAdapter.DataObjectHolder holder, int position) {

            final YearListObject.YearBean rowBean = mDataset.get(position);
            holder.tvYear.setText(rowBean.getTitle());



            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Global.yearBean = rowBean;
                    Constant.callActivity(mActivity, PlantActivity.class, null, false);
                }
            });
        }

        public void addItem(YearListObject.YearBean dataObj, int index) {
            mDataset.add(dataObj);
            notifyItemInserted(index);

        }

        public void deleteItem(int index) {
            mDataset.remove(index);
            notifyItemRemoved(index);
        }

        @Override
        public int getItemCount() {
            return mDataset.size();
        }
    }


}
