package com.vnerds.solarpanel.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.vnerds.custom.SharedPrefrenceUtil;
import com.vnerds.custom.SnEditText;
import com.vnerds.solarpanel.R;
import com.vnerds.library.Constant;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OTPVerificationActivity extends BaseActivity {


    @BindView(R.id.edtOTPNumber)
    SnEditText edtOTPNumber;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    FirebaseAuth mAuth ;
    String verificationToken ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otpverify);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);

        mAuth = FirebaseAuth.getInstance();
        verificationToken = getIntent().getStringExtra("api_key");


    }
    @OnClick(R.id.btnVerifyOTP)
    public void onClickVerifyOTP(View view) {
        if (isEditTextValid(edtOTPNumber, getString(R.string.validation_otp))) {
            verifyFirebase();
        }

    }
    @OnClick(R.id.tvResendCode)
    public void onclickResend(View view){
        callResendOTPAPI();
    }
    public void callResendOTPAPI() {
        if (isNetworkAvailable()) {
            showDialog();
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    getIntent().getStringExtra("code")+getIntent().getStringExtra("mobilenumber"),        // Phone number to verify
                    2,                 // Timeout duration
                    TimeUnit.MINUTES,   // Unit of timeout
                    this,               // Activity (for callback binding)
                    new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                        @Override
                        public void onVerificationCompleted(PhoneAuthCredential credential) {
                            hideDialog();
                        }

                        @Override
                        public void onVerificationFailed(FirebaseException e) {
                            hideDialog();
                            showToast(e.getMessage());
                        }

                        @Override
                        public void onCodeSent(String verificationId,
                                               PhoneAuthProvider.ForceResendingToken token) {
                            hideDialog();
                            showToast("OTP Sent Successfully.");
                            verificationToken = verificationId;
                        }
                    });

        }
    }

    public void verifyFirebase(){
        showDialog();
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationToken, getEditText(edtOTPNumber));
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = task.getResult().getUser();
                            String token = verificationToken;
                            SharedPrefrenceUtil.setString(getApplicationContext(),getString(R.string.app_name),getString(R.string.token),token);
                            Constant.callActivity(OTPVerificationActivity.this,ChannelsActivity.class,null,true);
                        } else {
                            hideDialog();
                            showToast(task.getException().toString());

                        }
                    }
                });

    }




}