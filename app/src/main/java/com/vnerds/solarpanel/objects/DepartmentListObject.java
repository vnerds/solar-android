package com.vnerds.solarpanel.objects;

import java.util.List;

/**
 * Created by home on 2/13/2018.
 */

public class DepartmentListObject extends CommonResponseObject{


    private String total;
    private List<DepartmentBean> department;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<DepartmentBean> getDepartment() {
        return department;
    }

    public void setDepartment(List<DepartmentBean> department) {
        this.department = department;
    }

    public static class DepartmentBean {
        private int start_num;
        private String department_id;
        private String name;
        private String uname;
        private String city;
        private String address;
        private String date_added;

        public int getStart_num() {
            return start_num;
        }

        public void setStart_num(int start_num) {
            this.start_num = start_num;
        }

        public String getDepartment_id() {
            return department_id;
        }

        public void setDepartment_id(String department_id) {
            this.department_id = department_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUname() {
            return uname;
        }

        public void setUname(String uname) {
            this.uname = uname;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getDate_added() {
            return date_added;
        }

        public void setDate_added(String date_added) {
            this.date_added = date_added;
        }
    }
}
