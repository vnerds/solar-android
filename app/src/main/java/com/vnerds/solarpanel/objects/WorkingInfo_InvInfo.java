package com.vnerds.solarpanel.objects;

public class WorkingInfo_InvInfo extends CommonResponseObject {

    private PlantBean plant;

    public PlantBean getPlant() {
        return plant;
    }

    public void setPlant(PlantBean plant) {
        this.plant = plant;
    }

    public static class PlantBean {
        private InvInfoBean inv_info;

        public InvInfoBean getInv_info() {
            return inv_info;
        }

        public void setInv_info(InvInfoBean inv_info) {
            this.inv_info = inv_info;
        }

        public static class InvInfoBean {
            private int total_device;
            private int total_faulty_device;
            private int total_running_device;
            private String device_faulty_rate;

            public int getTotal_device() {
                return total_device;
            }

            public void setTotal_device(int total_device) {
                this.total_device = total_device;
            }

            public int getTotal_faulty_device() {
                return total_faulty_device;
            }

            public void setTotal_faulty_device(int total_faulty_device) {
                this.total_faulty_device = total_faulty_device;
            }

            public int getTotal_running_device() {
                return total_running_device;
            }

            public void setTotal_running_device(int total_running_device) {
                this.total_running_device = total_running_device;
            }

            public String getDevice_faulty_rate() {
                return device_faulty_rate;
            }

            public void setDevice_faulty_rate(String device_faulty_rate) {
                this.device_faulty_rate = device_faulty_rate;
            }
        }
    }
}
