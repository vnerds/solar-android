package com.vnerds.solarpanel.activity;

import android.app.FragmentTransaction;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.vnerds.solarpanel.R;
import com.vnerds.solarpanel.fragments.GraphFragment;

import java.io.IOException;
import java.io.InputStream;

public class FragmentViewActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragmentview);

     /*   final FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frameLayout, new GraphFragment(), "NewFragmentTag");
        ft.commit();
*/


        GraphFragment detailsFragment = new GraphFragment();
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout,detailsFragment);
        fragmentTransaction.commit();
    }


}
