package com.vnerds.solarpanel.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.vnerds.custom.SnButton;
import com.vnerds.custom.SnEditText;
import com.vnerds.library.Constant;
import com.vnerds.solarpanel.R;
import com.vnerds.solarpanel.apis.APIClass;
import com.vnerds.solarpanel.application.SolarPanelApplication;
import com.vnerds.solarpanel.objects.Global;
import com.vnerds.solarpanel.objects.LoginObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddStateActivity extends BaseActivity {


    @BindView(R.id.edtName)
    SnEditText edtName;

    @BindView(R.id.edtDescription)
    SnEditText edtDescription;

    @BindView(R.id.edtUserName)
    SnEditText edtUserName;

    @BindView(R.id.edtPassword)
    SnEditText edtPassword;

    @BindView(R.id.btnAdd)
    SnButton btnAdd;

    LoginObject userObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_state);
        ButterKnife.bind(this);
        String title = getResources().getString(R.string.addstate);
        if (Global.stateBean != null) {
            title = getString(R.string.edit_oem);
            edtName.setText(Global.stateBean.getName());
            btnAdd.setText(getResources().getString(R.string.update));
        }

        ActionBar actionBar = getSupportActionBar();
        actionBar.show();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(title);
        userObject = getUserObject();


      /*  edtUserName.setText(userObject.getUser().getUname());
        edtUserName.setEnabled(false);
        edtUserName.setClickable(false);*/


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Global.stateBean = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @OnClick(R.id.btnAdd)
    public void onCLickLogin(View view) {
        if (isEditTextValid(edtName, getString(R.string.validation_name))) {
           // if (isEditTextValid(edtDescription, getString(R.string.validation_address)))
            {
            //    if (isEditTextValid(edtUserName, getString(R.string.validation_username)))
                {

                        callAddOEMAPI();


                }
            }
        }
    }


    public void callAddOEMAPI() {
        if (isNetworkAvailable()) {
            showDialog();

            Call<LoginObject> call = SolarPanelApplication.getApiServices().addState(getEditText(edtName), Global.oemBean.getOem_id(), userObject.getToken(), APIClass.ADD);
            if (Global.stateBean != null) {
                call = SolarPanelApplication.getApiServices().editState(Global.oemBean.getOem_id(),Global.stateBean.getState_id(), getEditText(edtName), userObject.getToken(), APIClass.EDIT);
            }
            call.enqueue(new Callback<LoginObject>() {
                @Override
                public void onResponse(Call<LoginObject> call, Response<LoginObject> response) {
                    hideDialog();

                    LoginObject object = response.body();

                    if (object != null && object.getError() == 0 && isValidResponse(object)) {
                        Constant.isOEMAdded = true;
                        showToastWithExit(object.getSuccess_msg());
                    } else if (response.errorBody() != null) {
                        try {
                            showErrorMsg(response.errorBody().string(), response.code());
                        } catch (Exception e) {

                        }
                    } else if (object != null && object.getError() == 1) {
                        showToast(object.getError_msg());
                    }
                }
                @Override
                public void onFailure(Call<LoginObject> call, Throwable t) {
                    hideDialog();
                    showToast(t.getMessage());
                    //listView.onRefreshComplete();
                    Log.d("CallBack", " Throwable is " + t);
                }
            });
        }
    }


    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();

    }


}