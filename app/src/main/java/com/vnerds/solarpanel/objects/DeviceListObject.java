package com.vnerds.solarpanel.objects;

import java.util.List;

public class DeviceListObject extends CommonResponseObject {


    private PlantBean plant;

    public PlantBean getPlant() {
        return plant;
    }

    public void setPlant(PlantBean plant) {
        this.plant = plant;
    }

    public static class PlantBean {


        private List<DeviceListBean> device_list;
        private SavingInfoBean saving_info;

        public List<DeviceListBean> getDevice_list() {
            return device_list;
        }

        public void setDevice_list(List<DeviceListBean> device_list) {
            this.device_list = device_list;
        }

        public SavingInfoBean getSaving_info() {
            return saving_info;
        }

        public void setSaving_info(SavingInfoBean saving_info) {
            this.saving_info = saving_info;
        }

        public static class DeviceListBean {

            public String id;
            public String device_name;
            public String plant_name;
            public String total_energy;
            public String status;
            public String InverterID;
            public String inv_id;

            public String getInverterID() {
                return InverterID;
            }

            public void setInverterID(String inverterID) {
                InverterID = inverterID;
            }

            public String getInv_id() {
                return inv_id;
            }

            public void setInv_id(String inv_id) {
                this.inv_id = inv_id;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getDevice_name() {
                return device_name;
            }

            public void setDevice_name(String device_name) {
                this.device_name = device_name;
            }

            public String getPlant_name() {
                return plant_name;
            }

            public void setPlant_name(String plant_name) {
                this.plant_name = plant_name;
            }

            public String getTotal_energy() {
                return total_energy;
            }

            public void setTotal_energy(String total_energy) {
                this.total_energy = total_energy;
            }
        }

        public static class SavingInfoBean {
            private double Total_Money_Saving;
            private double Today_Money_Saving;
            private String Today_Co2_Saving;

            public double getTotal_Money_Saving() {
                return Total_Money_Saving;
            }

            public void setTotal_Money_Saving(double Total_Money_Saving) {
                this.Total_Money_Saving = Total_Money_Saving;
            }

            public double getToday_Money_Saving() {
                return Today_Money_Saving;
            }

            public void setToday_Money_Saving(double Today_Money_Saving) {
                this.Today_Money_Saving = Today_Money_Saving;
            }

            public String getToday_Co2_Saving() {
                return Today_Co2_Saving;
            }

            public void setToday_Co2_Saving(String Today_Co2_Saving) {
                this.Today_Co2_Saving = Today_Co2_Saving;
            }
        }
    }
}
