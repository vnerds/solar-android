package com.vnerds.solarpanel.objects;

import java.util.List;

/**
 * Created by home on 2/13/2018.
 */

public class OEMListObject extends CommonResponseObject{

    /**
     * oem : [{"start_num":1,"oem_id":"15","name":"Vimal","uname":"vimal_in","address":"Gandhinagar"},{"start_num":2,"oem_id":"14","name":"Topsun","uname":"topsun_in","address":"Gandhinagar"},{"start_num":3,"oem_id":"11","name":"kishan patel","uname":"kishan","address":"address"}]
     * total : 3
     */

    private String total;
    /**
     * start_num : 1
     * oem_id : 15
     * name : Vimal
     * uname : vimal_in
     * address : Gandhinagar
     */

    private List<OemBean> oem;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<OemBean> getOem() {
        return oem;
    }

    public void setOem(List<OemBean> oem) {
        this.oem = oem;
    }

    public static class OemBean {
        private int start_num;
        private String oem_id;
        private String name;
        private String uname;
        private String address;

        public int getStart_num() {
            return start_num;
        }

        public void setStart_num(int start_num) {
            this.start_num = start_num;
        }

        public String getOem_id() {
            return oem_id;
        }

        public void setOem_id(String oem_id) {
            this.oem_id = oem_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUname() {
            return uname;
        }

        public void setUname(String uname) {
            this.uname = uname;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }
    }
}
