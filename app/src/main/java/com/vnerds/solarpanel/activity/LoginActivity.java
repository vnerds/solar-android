package com.vnerds.solarpanel.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.vnerds.custom.SharedPrefrenceUtil;
import com.vnerds.custom.SnEditText;
import com.vnerds.custom.SnTextView;
import com.vnerds.solarpanel.R;
import com.vnerds.solarpanel.apis.APIClass;
import com.vnerds.solarpanel.application.SolarPanelApplication;
import com.vnerds.solarpanel.objects.CountryCode;
import com.vnerds.library.Constant;
import com.vnerds.solarpanel.objects.LoginObject;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {



    @BindView(R.id.edtUserName)
    SnEditText edtUserName;

    @BindView(R.id.edtPassword)
    SnEditText edtPassword;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        String title = getResources().getString(R.string.login);

        ActionBar actionBar = getSupportActionBar();
        actionBar.show();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(title);


    }










    @OnClick(R.id.btnLogin)
    public void onCLickLogin(View view) {
        if (isEditTextValid(edtUserName, getString(R.string.validation_username))) {
            if (isEditTextValid(edtPassword,getString(R.string.validation_password))) {
                callLoginAPI();
            }
        }

    }


    public void callLoginAPI() {
        if (isNetworkAvailable()) {
            showDialog();

            Call<LoginObject> call = SolarPanelApplication.getApiServices().login(getEditText(edtUserName),getEditText(edtPassword), APIClass.GCM_TOKEN);
            call.enqueue(new Callback<LoginObject>() {
                @Override
                public void onResponse(Call<LoginObject> call, Response<LoginObject> response) {
                    hideDialog();

                    LoginObject object = response.body();

                    if (object != null && object.getError() == 0 && isValidResponse(object)) {
                        SharedPrefrenceUtil.setCustomObject(getApplicationContext(),getResources().getString(R.string.app_name), APIClass.SHARED_LOGIN, object);
                        Constant.callActivity(LoginActivity.this,OEMActivity.class,null,true);
                    }else if(response.errorBody()!=null){
                        try {
                            showErrorMsg(response.errorBody().string(),response.code());
                        }catch (Exception e){

                        }
                    }else if (object != null && object.getError() == 1) {
                        showToast(object.getError_msg());
                    }
                }

                @Override
                public void onFailure(Call<LoginObject> call, Throwable t) {
                   hideDialog();
                    showToast(t.getMessage());
                    //listView.onRefreshComplete();
                    Log.d("CallBack", " Throwable is " + t);
                }
            });


        }
    }


    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();

    }


}