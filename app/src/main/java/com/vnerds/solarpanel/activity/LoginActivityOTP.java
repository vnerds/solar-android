package com.vnerds.solarpanel.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.vnerds.custom.SharedPrefrenceUtil;
import com.vnerds.custom.SnEditText;
import com.vnerds.custom.SnTextView;
import com.vnerds.library.Constant;
import com.vnerds.solarpanel.R;
import com.vnerds.solarpanel.application.SolarPanelApplication;
import com.vnerds.solarpanel.objects.CountryCode;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivityOTP extends BaseActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    private static final String TAG = LoginActivityOTP.class.getName();
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;

    @BindView(R.id.tvCountyCode)
    SnTextView tvCountyCode;

    @BindView(R.id.edtMobileNumber)
    SnEditText edtMobileNumber;

    @BindView(R.id.button_facebook_login)
    LoginButton loginButton ;


    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private String name, email,token;


    private CallbackManager callbackManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobilenumber);
        ButterKnife.bind(this);
        String title = getResources().getString(R.string.login);

        ActionBar actionBar = getSupportActionBar();
        actionBar.show();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(title);


        tvCountyCode.setText("");
        callForCountryCodeAPI();
        configureSignIn();

        callbackManager = CallbackManager.Factory.create();
        mAuth = com.google.firebase.auth.FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener(){
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    SharedPrefrenceUtil.setString(getApplicationContext(),getResources().getString(R.string.app_name),getString(R.string.token),user.getUid());
                    createUserInFirebaseHelper();
                } else {
                 //   showToast("onAuthStateChanged:signed_out");

                }

            }

        };
        loginButton.setReadPermissions("email");
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                signInWithFacebook(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
            }
        });
    }

    private void createUserInFirebaseHelper() {
        Constant.callActivity(LoginActivityOTP.this,ChannelsActivity.class,null,true);
    }


    public void configureSignIn(){

    GoogleSignInOptions options = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
               // .requestIdToken(getString(R.string.webclientid))
                .build();
       mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, options)
                .build();

    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void firebaseAuthWithGoogle(AuthCredential credential){
       showDialog();
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential" + task.getException().getMessage());
                            task.getException().printStackTrace();
                            showToast("Authentication failed.");
                        }else {


                        }
                       hideDialog();
                    }
                });
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount account = result.getSignInAccount();
                 token = account.getIdToken();
                 name = account.getDisplayName();
                 email = account.getEmail();

                AuthCredential credential = GoogleAuthProvider.getCredential(token, null);
                firebaseAuthWithGoogle(credential);

            } else {
                showToast("Login Unsuccessful");

            }

        }

    }
    private void signInWithFacebook(AccessToken token) {
        Log.d(TAG, "signInWithFacebook:" + token);

        showDialog();

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            showToast("Authentication failed.");
                        }else{
                            String uid=task.getResult().getUser().getUid();
                            SharedPrefrenceUtil.setString(getApplicationContext(),getString(R.string.app_name),getString(R.string.token),uid);
                            createUserInFirebaseHelper();
                        }

                        hideDialog();
                    }
                });
    }

    @OnClick(R.id.btnSendOTP)
    public void onCLickSendOTP(View view) {
        if (isEditTextValid(edtMobileNumber, getString(R.string.validation_mobile))) {
            if (isValidMobile(edtMobileNumber,getString(R.string.validation_valid_mobile))) {
                callMobileVerify(getEditText(edtMobileNumber));
            }
        }

    } @OnClick(R.id.login_with_google)
    public void onCllickGoogleLogin(View view) {
        if (isNetworkAvailable()){
            signIn();
        }
    }

    public void callMobileVerify(String mobileNumber) {
        if (isNetworkAvailable()) {
            showDialog();
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                    getTextValue(tvCountyCode)+getEditText(edtMobileNumber),        // Phone number to verify
                    2,                 // Timeout duration
                    TimeUnit.MINUTES,   // Unit of timeout
                    this,               // Activity (for callback binding)
                    new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                        @Override
                        public void onVerificationCompleted(PhoneAuthCredential credential) {
                           hideDialog();

                        }

                        @Override
                        public void onVerificationFailed(FirebaseException e) {
                           hideDialog();
                            showToast(e.getMessage());

                        }

                        @Override
                        public void onCodeSent(String verificationId,
                                               PhoneAuthProvider.ForceResendingToken token) {
                            // Log.d(TAG, "onCodeSent:" + verificationId);
                            Bundle bundle = new Bundle();
                            bundle.putString("mobilenumber", getEditText(edtMobileNumber));
                            bundle.putString("api_key", verificationId);
                            bundle.putString("code", getTextValue(tvCountyCode));
                            Constant.callActivity(LoginActivityOTP.this,OTPVerificationActivity.class, bundle,true);
                            String    mVerificationId = verificationId;
                            PhoneAuthProvider.ForceResendingToken   mResendToken = token;
                        }
                    });

        }
    }

    public void callForCountryCodeAPI() {
        if (isNetworkAvailable()) {
            showDialog();

            Call<CountryCode> call =null ;//SolarPanelApplication.getApiServices().getCountryCode();
            call.enqueue(new Callback<CountryCode>() {
                @Override
                public void onResponse(Call<CountryCode> call, Response<CountryCode> response) {
                    hideDialog();

                    CountryCode object = response.body();

                    if (object != null) {
                       tvCountyCode.setText("+"+ Constant.getCountryDialCode(LoginActivityOTP.this,object.getCountryCode()));
                    }else if(response.errorBody()!=null){
                        try {
                            showErrorMsg(response.errorBody().string(),response.code());
                        }catch (Exception e){

                        }
                    }
                }

                @Override
                public void onFailure(Call<CountryCode> call, Throwable t) {
                   hideDialog();
                    showToast(t.getMessage());
                    //listView.onRefreshComplete();
                    Log.d("CallBack", " Throwable is " + t);
                }
            });


        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (mAuthListener != null){
            FirebaseAuth.getInstance().signOut();
        }
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null){
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}