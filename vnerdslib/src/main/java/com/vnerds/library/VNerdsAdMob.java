package com.vnerds.library;

import android.content.Context;

import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

/**
 * Created by parag on 7/26/2017.
 */

public class VNerdsAdMob {
    public void adMobInitialize(Context context ,String adMobId){
        MobileAds.initialize(context, adMobId);
    }

    public InterstitialAd adMob(Context context,String interstitialId){
        InterstitialAd  mInterstitialAd = new InterstitialAd(context);
        mInterstitialAd.setAdUnitId(interstitialId);
        return mInterstitialAd;
    }
}
