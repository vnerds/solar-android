package com.vnerds.solarpanel.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.gson.Gson;
import com.twinkle94.monthyearpicker.picker.YearMonthPickerDialog;
import com.vnerds.custom.SnTextView;
import com.vnerds.solarpanel.R;
import com.vnerds.solarpanel.activity.BaseActivity;
import com.vnerds.solarpanel.apis.APIClass;
import com.vnerds.solarpanel.application.SolarPanelApplication;
import com.vnerds.solarpanel.objects.Global;
import com.vnerds.solarpanel.objects.GraphObject;
import com.vnerds.solarpanel.objects.GraphObjectWithoutDay;
import com.vnerds.solarpanel.objects.LoginObject;
import com.vnerds.solarpanel.objects.WorkingInfoObject;
import com.vnerds.solarpanel.objects.WorkingInfo_InvInfo;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.hoang8f.android.segmented.SegmentedGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class GraphFragment extends Fragment implements RadioGroup.OnCheckedChangeListener {


    @BindView(R.id.btnDay)
    RadioButton btnDay;

    @BindView(R.id.btnMonth)
    RadioButton btnMonth;


    @BindView(R.id.btnYear)
    RadioButton btnYear;


    @BindView(R.id.btnTotal)
    RadioButton btnTotal;


    @BindView(R.id.segmented)
    SegmentedGroup segmented;


    @BindView(R.id.webView)
    WebView webView;

    @BindView(R.id.tvDate)
    SnTextView tvDate;


    String selectedType, chartType;
    int selectType = 0;

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, @IdRes int checkedId) {

        switch (checkedId) {
            case R.id.btnDay:

                chartType = "Output_Power";
                selectedType = btnDay.getText().toString();
                selectType = 0;
                break;
            case R.id.btnMonth:

                chartType = "Monthly_Energy";
                selectedType = btnMonth.getText().toString();
                selectType = 1;
                break;

            case R.id.btnYear:

                chartType = "Yearly_Energy";
                selectedType = btnYear.getText().toString();
                selectType = 2;
                break;
            case R.id.btnTotal:

                chartType = "Total_Energy";
                selectedType = btnTotal.getText().toString();
                selectType = 2;
                break;


            default:

        }
        webView.setVisibility(View.GONE);
        setDate();

    }

    public String getStringFromAsset() {


            BufferedReader in = null;
            try {
                StringBuilder buf = new StringBuilder();
                InputStream is = getActivity().getAssets().open("month.txt");
                in = new BufferedReader(new InputStreamReader(is));

                String str;
                boolean isFirst = true;
                while ( (str = in.readLine()) != null ) {
                    if (isFirst)
                        isFirst = false;
                    else
                        buf.append('\n');
                    buf.append(str);
                }
                return buf.toString();
            } catch (IOException e) {
              e.printStackTrace();
            }
            return null;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_graph, container, false);
    }


    LoginObject login_object;


    Calendar calendar;
    int day, month, year;

    public String getFormattedString(int day) {
        String dayString = "";
        if (day < 10) {
            dayString = "0" + day;
        } else {
            dayString = "" + day;
        }
        return dayString;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(false);

        segmented.setTintColor((getResources().getColor(R.color.colorPrimaryDark)), Color.parseColor("#FFFFFF"));
        segmented.setOnCheckedChangeListener(this);


        calendar = Calendar.getInstance();

        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = calendar.get(Calendar.MONTH);
        ;
        year = calendar.get(Calendar.YEAR);

        login_object = BaseActivity.getUserObject();

        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                webView.setVisibility(View.GONE);
                switch (selectType) {
                    case 0:
                        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), datePickerListener, year, month, day);
                        datePickerDialog.show();
                        break;
                    case 1:
                        selectYear();
                        break;
                    case 2:
                        selectYear();
                        break;
                }
            }
        });


        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        webView.clearCache(true);
        settings.setDomStorageEnabled(true);

       /* webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                webView.loadUrl(
                        "javascript:(function() { "  +getStringFromAsset()+
                                "})()");
            }
        });
        webView.loadUrl("file:///android_asset/chart.html");*/
        btnDay.setChecked(true);
        //webView.setWebViewClient(new WebViewClient());


       /* String html = "<html>\n" +
                "\n" +
                "<body>\n" +
                "    <script src=\"https://code.highcharts.com/highcharts.js\"></script>\n" +
                "    <script src=\"https://code.highcharts.com/modules/exporting.js\"></script>\n" +
                getStringFromAsset() +
                "\t<div id=\"container\" style=\"min-width: 310px; height: 400px; background-color:lightgrey; margin: 0 auto \"></div>";
   */   //   webView.loadDataWithBaseURL("file:///android_asset/chart.html", getStringFromAsset(), "text/html", "utf-8", "");
        //   MyJavaScriptInterface javaInterface = new MyJavaScriptInterface();
        //  webView.addJavascriptInterface(javaInterface, "highcharts1.js");
        //webView.loadData(html,"text/html", "UTF-8");
        //  webView.loadUrl("javascript:( function () { var resultSrc = document.getElementById(\"image\").getAttribute(\"src\"); window.HTMLOUT.someCallback(resultSrc); } ) ()");


        //  webView.loadUrl("file:///android_asset/chart.html");


    }

    class MyJavaScriptInterface {
        @JavascriptInterface
        public void someCallback(String jsResult) {
            // your code...
        }
    }

    public void setDate() {
        switch (selectType) {
            case 0:
                tvDate.setText(getFormattedString(day) + "-" + getFormattedString(month + 1) + "-" + year);
                getPlantInvGraph();
                break;
            case 1:
                tvDate.setText(getFormattedString(month + 1) + "/" + year);
                getPlantInvGraphByOne();
                break;
            case 2:
                tvDate.setText("" + year);
                getPlantInvGraphByOne();
                break;
        }
    }

    public String getCurrentDate() {

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        return String.valueOf(formatter.format(calendar.getTime()));
    }

    /* protected Dialog onCreateDialog(int id) {
         DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), datePickerListener, year, month, day);
         return datePickerDialog ;
     }
 */
    public void selectYear() {

        YearMonthPickerDialog yearMonthPickerDialog = new YearMonthPickerDialog(getActivity(),
                new YearMonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onYearMonthSet(int yea, int mont) {
              /* Calendar calendar = Calendar.getInstance();
               calendar.set(Calendar.YEAR, year);
               calendar.set(Calendar.MONTH, month);*/
                        year = yea;
                        month = mont;
                        setDate();
                        //  SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM yyyy");
                        // tvDate.setText( getFormattedString(month + 1) + "/"     + year);

                    }
                });

        yearMonthPickerDialog.show();
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            day = selectedDay;
            month = selectedMonth;
            year = selectedYear;
            tvDate.setText(getFormattedString(selectedDay) + "-" + getFormattedString(selectedMonth + 1) + "-"
                    + selectedYear);
            getPlantInvGraph();
        }
    };

    public void getPlantInvGraph() {
        if (BaseActivity.isNetworkAvailable()) {
            BaseActivity.showDialog();
            //http://117.247.80.164:85/herapi/plant_inv_graph.php?tag=graph&id=6985&key=day&val=15-05-2019&chart_type=Output_Power&token=ey
          /*@Query(APIClass.ID) String id,
            @Query(APIClass.KEY) String key,
            @Query(APIClass.VAL) String val,
            @Query(APIClass.CHART_TYPE) String chart_type,
            @Query(APIClass.TOKEN) String token,
            @Query(APIClass.TAG) String tag*/
            /*Call<GraphObject> call = SolarPanelApplication.getApiServices().getPlantInvGraph(Global.graphPlantBean.getId(),
                    selectedType.toLowerCase(),tvDate.getText().toString(),chartType,
                    login_object.getToken(), APIClass.GRAPH);*/
            Call<GraphObject> call = SolarPanelApplication.getApiServices().getPlantInvGraph(Global.graphPlantBean.getId(),
                    selectedType.toLowerCase(), tvDate.getText().toString(), chartType,
                    login_object.getToken(), APIClass.GRAPH);
            call.enqueue(new Callback<GraphObject>() {
                @Override
                public void onResponse(Call<GraphObject> call, Response<GraphObject> response) {
                    BaseActivity.hideDialog();
                    final GraphObject oemListObject = response.body();
                    if (BaseActivity.isValidResponse(oemListObject)) {
                        withDay(oemListObject);
                    } else if (response.errorBody() != null) {
                        try {
                            BaseActivity.showErrorMsg(response.errorBody().string(), response.code());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }

                @Override
                public void onFailure(Call<GraphObject> call, Throwable t) {
                    BaseActivity.hideDialog();
                //    BaseActivity.showToast(t.getMessage());
                }
            });

        }
    }

    public void getPlantInvGraphByOne() {
        if (BaseActivity.isNetworkAvailable()) {
            BaseActivity.showDialog();
            //http://117.247.80.164:85/herapi/plant_inv_graph.php?tag=graph&id=6985&key=day&val=15-05-2019&chart_type=Output_Power&token=ey
          /*@Query(APIClass.ID) String id,
            @Query(APIClass.KEY) String key,
            @Query(APIClass.VAL) String val,
            @Query(APIClass.CHART_TYPE) String chart_type,
            @Query(APIClass.TOKEN) String token,
            @Query(APIClass.TAG) String tag*/
            Call<GraphObjectWithoutDay> call = SolarPanelApplication.getApiServices().getPlantInvGraphWithoutDay(Global.graphPlantBean.getId(),
                    selectedType.toLowerCase(), tvDate.getText().toString(), chartType,
                    login_object.getToken(), APIClass.GRAPH);
            call.enqueue(new Callback<GraphObjectWithoutDay>() {
                @Override
                public void onResponse(Call<GraphObjectWithoutDay> call, Response<GraphObjectWithoutDay> response) {
                    BaseActivity.hideDialog();
                    final GraphObjectWithoutDay oemListObject = response.body();
                    if (BaseActivity.isValidResponse(oemListObject)) {
                        withoutDay(oemListObject);
                    } else if (response.errorBody() != null) {
                        try {
                            BaseActivity.showErrorMsg(response.errorBody().string(), response.code());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }

                @Override
                public void onFailure(Call<GraphObjectWithoutDay> call, Throwable t) {
                    BaseActivity.hideDialog();
                    BaseActivity.showToast(t.getMessage());
                }
            });

        }
    }


    @Override
    public void onResume() {
        super.onResume();


    }


    public Context getApplicationContext() {
        return getActivity();
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onPause() {

        super.onPause();
    }

    public void withDay(GraphObject graphObject) {

        GraphObject graphInfo = graphObject;

        if (graphInfo != null) {
            GraphObject.GraphDataBean graphDataBean = graphObject.getGraph_data();

            if (graphDataBean == null) {
                webView.setVisibility(View.GONE);
                return;
            }

            List<String> invertorId = graphDataBean.getInverterID();
            if (invertorId == null) {
                webView.setVisibility(View.GONE);
                return;
            } else if (invertorId.size() == 0) {
                webView.setVisibility(View.GONE);
                return;
            }

            GraphObject.GraphDataBean.InverterInfoDataBean inverter_info_data = graphDataBean.getInverter_info_data();
            if (inverter_info_data == null) {
                webView.setVisibility(View.GONE);
                return;
            }

            webView.setVisibility(View.VISIBLE);

            List<GraphObject.GraphDataBean.InverterInfoDataBean.F01175008453Bean> dataF011 = inverter_info_data.getF01175008453();
            List<String> data = new ArrayList<>();
            JSONArray mainJsonArray = new JSONArray();
            String name = "";
            for (int i = 0; i < invertorId.size(); i++) {
                String dic = invertorId.get(i);


                try {
                    JSONObject mainJsonObject = new JSONObject();
                    JSONArray seriesJsonArray = new JSONArray();


                    name = dic;
                    List<GraphObject.GraphDataBean.InverterInfoDataBean.F01175008453Bean> arrayData = new ArrayList<>();
                    String stringFinalGraphInput = "";
                    for (int j = 0; j < graphDataBean.getInverter_info_data().getF01175008453().size(); j++) {
                        GraphObject.GraphDataBean.InverterInfoDataBean.F01175008453Bean obj = graphDataBean.getInverter_info_data().getF01175008453().get(j);
                        String year = (obj.getY());
                        String month = obj.getN();
                        String date = obj.getJ();
                        String hour = obj.getG();
                        String minutes = obj.getI();
                        String second = obj.getS();
                        String Output_Power = obj.getOutput_Power();
                        //[Date.UTC(2019,5,11,5,50,45),0]
                        String stringItem = "[Date.UTC(" + year + "," + month + "," + date + "," + hour + "," + minutes + "," + second + ")," + Output_Power + "]";

                        seriesJsonArray.put(stringItem);
                        data.add(stringItem);
                        stringFinalGraphInput += stringItem;
                    }

                    mainJsonObject.put("data", seriesJsonArray);
                    mainJsonObject.put("name", "'" + name + "'");
                    mainJsonArray.put(mainJsonObject);
                    Gson gson = new Gson();
                    String dataInfo = mainJsonArray.toString();//gson.toJson(mainJsonArray);

                 /*   let mainDic = "\(dic)"
                    let removeFirst = mainDic.chopPrefix()
                    let removeLast = removeFirst.chopSuffix()
                    let removeString = "{\(removeLast)}"
*/
                    dataInfo = dataInfo.replaceAll("\"", "");

                    // dataInfo = "[{name: \\'F01175008453\\', data: [[Date.UTC(2019,5,14,14,11,00),3.461], [Date.UTC(2019,5,14,14,13,00),3.512], [Date.UTC(2019,5,14,14,15,01),2.156], [Date.UTC(2019,5,14,14,17,01),2.262], [Date.UTC(2019,5,14,14,19,01),3.464], [Date.UTC(2019,5,14,14,21,02),3.584], [Date.UTC(2019,5,14,14,23,02),3.457], [Date.UTC(2019,5,14,14,25,03),2.751], [Date.UTC(2019,5,14,14,27,03),3.038], [Date.UTC(2019,5,14,14,29,03),3.122], [Date.UTC(2019,5,14,14,31,04),2.937], [Date.UTC(2019,5,14,14,33,04),1.982], [Date.UTC(2019,5,14,14,35,05),1.225], [Date.UTC(2019,5,14,14,37,05),1.363], [Date.UTC(2019,5,14,14,39,06),3.074], [Date.UTC(2019,5,14,14,41,06),3.572], [Date.UTC(2019,5,14,14,43,06),2.911], [Date.UTC(2019,5,14,14,45,07),3.476], [Date.UTC(2019,5,14,14,47,06),2.013], [Date.UTC(2019,5,14,14,49,07),1.408], [Date.UTC(2019,5,14,14,51,07),3.392], [Date.UTC(2019,5,14,14,53,07),2.844], [Date.UTC(2019,5,14,14,55,08),3.357], [Date.UTC(2019,5,14,14,57,08),3.408], [Date.UTC(2019,5,14,14,59,08),3.17], [Date.UTC(2019,5,14,15,01,09),1.364], [Date.UTC(2019,5,14,15,03,09),1.583], [Date.UTC(2019,5,14,15,05,09),3.051], [Date.UTC(2019,5,14,15,07,10),2.63], [Date.UTC(2019,5,14,15,09,10),2.457], [Date.UTC(2019,5,14,15,11,11),2.929], [Date.UTC(2019,5,14,15,13,11),2.442], [Date.UTC(2019,5,14,15,15,11),2.423], [Date.UTC(2019,5,14,15,17,12),2.463], [Date.UTC(2019,5,14,15,19,12),1.691], [Date.UTC(2019,5,14,15,21,13),1.806], [Date.UTC(2019,5,14,15,23,13),1.858], [Date.UTC(2019,5,14,15,25,13),1.721], [Date.UTC(2019,5,14,15,27,14),2.124], [Date.UTC(2019,5,14,15,29,14),1.813], [Date.UTC(2019,5,14,15,31,15),1.948], [Date.UTC(2019,5,14,15,33,15),1.878], [Date.UTC(2019,5,14,15,35,15),1.797], [Date.UTC(2019,5,14,15,37,16),1.97], [Date.UTC(2019,5,14,15,39,16),1.835], [Date.UTC(2019,5,14,15,41,17),1.747], [Date.UTC(2019,5,14,15,43,17),1.619], [Date.UTC(2019,5,14,15,45,18),1.455], [Date.UTC(2019,5,14,15,47,18),1.288], [Date.UTC(2019,5,14,15,49,18),1.091], [Date.UTC(2019,5,14,15,51,19),1.147], [Date.UTC(2019,5,14,15,53,19),1.187], [Date.UTC(2019,5,14,15,55,19),1.176], [Date.UTC(2019,5,14,15,57,20),1.16], [Date.UTC(2019,5,14,15,59,20),1.191], [Date.UTC(2019,5,14,16,01,21),1.299], [Date.UTC(2019,5,14,16,03,21),1.428], [Date.UTC(2019,5,14,16,05,22),1.631], [Date.UTC(2019,5,14,16,07,22),1.687], [Date.UTC(2019,5,14,16,09,23),1.777], [Date.UTC(2019,5,14,16,11,23),1.957], [Date.UTC(2019,5,14,16,13,24),2.055], [Date.UTC(2019,5,14,16,15,24),2.158], [Date.UTC(2019,5,14,16,17,24),2.266], [Date.UTC(2019,5,14,16,19,25),2.349], [Date.UTC(2019,5,14,16,21,25),2.474], [Date.UTC(2019,5,14,16,23,26),2.472], [Date.UTC(2019,5,14,16,25,26),2.509], [Date.UTC(2019,5,14,16,27,26),2.561], [Date.UTC(2019,5,14,16,29,27),2.59], [Date.UTC(2019,5,14,16,31,27),2.596], [Date.UTC(2019,5,14,16,33,27),2.483], [Date.UTC(2019,5,14,16,35,28),2.511], [Date.UTC(2019,5,14,16,37,28),2.459], [Date.UTC(2019,5,14,16,39,28),2.306], [Date.UTC(2019,5,14,16,41,29),2.31], [Date.UTC(2019,5,14,16,43,29),2.258], [Date.UTC(2019,5,14,16,45,29),2.166], [Date.UTC(2019,5,14,16,47,30),1.955], [Date.UTC(2019,5,14,16,49,30),1.876], [Date.UTC(2019,5,14,16,51,30),1.811], [Date.UTC(2019,5,14,16,53,31),1.804], [Date.UTC(2019,5,14,16,55,31),1.809], [Date.UTC(2019,5,14,16,57,31),1.818], [Date.UTC(2019,5,14,16,59,32),1.774], [Date.UTC(2019,5,14,17,01,32),1.717], [Date.UTC(2019,5,14,17,03,33),1.617], [Date.UTC(2019,5,14,17,05,33),1.393], [Date.UTC(2019,5,14,17,07,33),1.298], [Date.UTC(2019,5,14,17,09,34),1.147], [Date.UTC(2019,5,14,17,11,33),1.012], [Date.UTC(2019,5,14,17,13,34),0.877], [Date.UTC(2019,5,14,17,15,34),0.9360000000000001], [Date.UTC(2019,5,14,17,17,35),0.869], [Date.UTC(2019,5,14,17,19,35),1.03], [Date.UTC(2019,5,14,17,21,35),1.034], [Date.UTC(2019,5,14,17,23,36),1.087], [Date.UTC(2019,5,14,17,25,36),1.088], [Date.UTC(2019,5,14,17,27,37),1.056], [Date.UTC(2019,5,14,17,29,37),1.042], [Date.UTC(2019,5,14,17,31,38),0.998], [Date.UTC(2019,5,14,17,33,38),0.896], [Date.UTC(2019,5,14,17,35,39),0.856], [Date.UTC(2019,5,14,17,37,39),0.799], [Date.UTC(2019,5,14,17,39,40),0.772], [Date.UTC(2019,5,14,17,41,40),0.725], [Date.UTC(2019,5,14,17,43,40),0.695], [Date.UTC(2019,5,14,17,45,41),0.664], [Date.UTC(2019,5,14,17,47,41),0.649], [Date.UTC(2019,5,14,17,49,41),0.643], [Date.UTC(2019,5,14,17,51,42),0.616], [Date.UTC(2019,5,14,17,53,42),0.584], [Date.UTC(2019,5,14,17,55,43),0.552], [Date.UTC(2019,5,14,17,57,43),0.524], [Date.UTC(2019,5,14,17,59,43),0.51], [Date.UTC(2019,5,14,18,01,44),0.492], [Date.UTC(2019,5,14,18,03,44),0.478], [Date.UTC(2019,5,14,18,05,44),0.461], [Date.UTC(2019,5,14,18,07,45),0.43]]}]";
                    //     String arrayStr = stringFinalGraphInput.substring(0,stringFinalGraphInput.length()-1);
                    // String dataInfo = mainJsonArray.toString() ;

                    String stringScript = "Highcharts.chart(\'container\',{chart:{type:\'areaspline\',pinchType : \'xy\', zoomType: \'xy\', panning: true},title:{text:\'\'},exporting: { enabled: false },subtitle:{text:\'\'},xAxis:{type:\'datetime\',dateTimeLabelFormats:{month:\'%e. %b\',year:\'%b\'},title:{text:\'Date\'}},yAxis:{title:{text:\'\'},min:0},tooltip:{headerFormat:\'<b>{series.name}</b><br>\',pointFormat:\'{point.x:%e. %b}: {point.y:.2f} m\'},plotOptions: {areaspline: {fillOpacity: 0.5}},series:" + dataInfo + "});";
                    // webView.loadData(stringScript,"text/html", "utf-8");
                    // WebSettings webSettings = webView.getSettings();
                    webView.setWebViewClient(new WebViewClient() {
                        @Override
                        public void onPageFinished(WebView view, String url) {
                            super.onPageFinished(view, url);
                            webView.loadUrl(
                                    "javascript:(function() { " + stringScript +
                                            "})()");
                        }
                    });
                    webView.loadUrl("file:///android_asset/chart.html");
                    // webSettings.setJavaScriptEnabled(true);
                    //   webView.loadDataWithBaseURL("file:///android_asset/www", stringScript, "text/html", "utf-8", "");


                } catch (Exception e) {
                    e.printStackTrace();
                }


            }


        } else {
            webView.setVisibility(View.GONE);
        }


    }

    public void withoutDay(GraphObjectWithoutDay graphObject) {

        GraphObjectWithoutDay graphInfo = graphObject;

        if (graphInfo != null) {
            GraphObjectWithoutDay.GraphDataBean graphDataBean = graphObject.getGraph_data();

            if (graphDataBean == null) {
                webView.setVisibility(View.GONE);
                return;
            }

            List<String> invertorId = graphDataBean.getInverterID();
            if (invertorId == null) {
                webView.setVisibility(View.GONE);
                return;
            } else if (invertorId.size() == 0) {
                webView.setVisibility(View.GONE);
                return;
            }

            GraphObjectWithoutDay.GraphDataBean.SeriesBean seariesBean = graphDataBean.getSeries();
            if (seariesBean == null) {
                webView.setVisibility(View.GONE);
                return;
            }

            webView.setVisibility(View.VISIBLE);



            //  "graph_data":{"InverterID":["F01175008453"],"xAxis":["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"],"series":{"F01175008453":[24.86,24.83,24.42,24.09,26.16,17.21,26.82,27.29,25.28,26.79,3.91,20.05,26.57,21.14,25.98,25.78,0,0,0,26.45,26.21,26.44,25.08,25.85,24.62,24.92,25.6,25.15,25.45,25.47,24.98]}}
            List<String> arrayXDataNewyork = graphDataBean.getXAxis();
            GraphObjectWithoutDay.GraphDataBean.SeriesBean arrayXDataTokyo = graphDataBean.getSeries();
            JSONArray mainJsonArray = new JSONArray();
            List<String> info = new ArrayList<>();
            String name = "";

            JSONArray seriesJsonArray = new JSONArray();
            JSONArray xJSONArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();


            for (int i = 0; i < invertorId.size(); i++) {
                String seriesname = invertorId.get(i);


                try {
                    List<Double> listSeriesValue = seariesBean.getF01175008453();
                    for (int j = 0; j < listSeriesValue.size(); j++) {
                        seriesJsonArray.put(listSeriesValue.get(j));
                    }
                    for (int j = 0; j < graphDataBean.getXAxis().size(); j++) {
                        xJSONArray.put(graphDataBean.getXAxis().get(j)+"");
                    }
                    jsonObject.put("name",   seriesname );
                    jsonObject.put("data", seriesJsonArray);

                    mainJsonArray.put(jsonObject);


                    String series = mainJsonArray.toString();//.replace("\""+seriesname+"\"","\'"+seriesname+"\'");
                    String xArray = xJSONArray.toString() ;
                    String stringScript = "Highcharts.chart(\'container\',{chart:{type:\'column\',pinchType : \'xy\', zoomType: \'xy\', panning: true},exporting: { enabled: false },xAxis:{categories:" +xArray + ",title:{text:null},scrollbar:{ enabled: true}},yAxis:{min:0,title:{text:\'\',align:\'high\'},labels:{overflow:\'justify\'},scrollbar:{ enabled: true}},tooltip:{valueSuffix:\' millions\'},plotOptions:{bar:{dataLabels:{enabled:true}}},legend:{layout:\'vertical\',align:\'right\',verticalAlign:\'top\',x:-40,y:80,floating:true,borderWidth:1,backgroundColor:((Highcharts.theme&&Highcharts.theme.legendBackgroundColor)||\'#FFFFFF\'),shadow:true},credits:{enabled:false},series:" + series + "});";
                    webView.setWebViewClient(new WebViewClient() {
                        @Override
                        public void onPageFinished(WebView view, String url) {
                            super.onPageFinished(view, url);
                            webView.loadUrl("javascript:(function() { " + stringScript +
                                            "})()");
                        }
                    });
                    webView.loadUrl("file:///android_asset/chart.html");


                } catch (Exception e) {
                    e.printStackTrace();
                }


            }


        } else {
            webView.setVisibility(View.GONE);
        }


    }

}
