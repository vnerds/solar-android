package com.vnerds.solarpanel.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.github.captain_miao.recyclerviewutils.BaseWrapperRecyclerAdapter;
import com.github.captain_miao.recyclerviewutils.common.BaseLoadMoreFooterView;
import com.github.captain_miao.recyclerviewutils.listener.LinearLayoutWithRecyclerOnScrollListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.vnerds.custom.SnTextView;
import com.vnerds.solarpanel.R;
import com.vnerds.solarpanel.objects.ChannelListObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;
import in.srain.cube.views.ptr.header.MaterialHeader;
import in.srain.cube.views.ptr.util.PtrLocalDisplay;


public class FavoritesActivity extends BaseActivity {

   @BindView(R.id.material_style_ptr_frame)
   PtrFrameLayout mPtrFrameLayout;

    private LinearLayoutWithRecyclerOnScrollListener mLoadMoreListener;

    SnTextView tvTitle;
    ImageView imgFav,imgSort,imgLogout;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.rvHome)
    RecyclerView rvHome;

    MyRecyclerViewAdapter myRecyclerViewAdapter;

    int pageCount = 0;

    String selectedFilter = "";
    LinearLayoutManager llm;
    FirebaseAuth mAuth ;
    FirebaseDatabase database;
    HashMap<Integer,ChannelListObject.ChannelsBean> channelsBeanHashMap;
    boolean isValueUpdated = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channels);

        ButterKnife.bind(this);
        channelsBeanHashMap = new HashMap<>();
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();

        tvTitle =(SnTextView) mToolbar.findViewById(R.id.tvTitle);
        imgFav =(ImageView)mToolbar.findViewById(R.id.imgFav);
        imgLogout =(ImageView)mToolbar.findViewById(R.id.imgLogout);
        imgSort =(ImageView)mToolbar.findViewById(R.id.imgSort);
        imgFav.setVisibility(View.GONE);
        imgLogout.setVisibility(View.GONE);
        imgSort.setVisibility(View.GONE);


        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        tvTitle.setText("Favorites");
        List<ChannelListObject.ChannelsBean> channelsBeanList = new ArrayList<>();
        myRecyclerViewAdapter = new MyRecyclerViewAdapter(channelsBeanList);

        rvHome.setAdapter(myRecyclerViewAdapter);
        rvHome.setNestedScrollingEnabled(false);
        llm = new LinearLayoutManager(this);
        rvHome.setLayoutManager(llm);



        myRecyclerViewAdapter.setLoadMoreFooterView(new BaseLoadMoreFooterView(this) {
            @Override
            public int getLoadMoreLayoutResource() {
                return R.layout.list_load_more;
            }
        });
        mLoadMoreListener = new LinearLayoutWithRecyclerOnScrollListener(llm) {

            @Override
            public void onLoadMore(final int pagination, final int pageSize) {

                rvHome.post(new Runnable() {
                    @Override
                    public void run() {
                        myRecyclerViewAdapter.showLoadMoreView();
                    }
                });

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadComplete();
                    }
                },2000);
            }
        };

        final MaterialHeader header = new MaterialHeader(this);

        int[] colors = getResources().getIntArray(R.array.google_colors);
        header.setColorSchemeColors(colors);
        header.setLayoutParams(new PtrFrameLayout.LayoutParams(-1, -2));
        header.setPadding(0, PtrLocalDisplay.dp2px(15), 0, PtrLocalDisplay.dp2px(10));
        header.setPtrFrameLayout(mPtrFrameLayout);


        mPtrFrameLayout.setHeaderView(header);
        mPtrFrameLayout.addPtrUIHandler(header);

        mPtrFrameLayout.setEnabledNextPtrAtOnce(true);
        mPtrFrameLayout.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                    return mLoadMoreListener.checkCanDoRefresh();
            }

            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {

                mPtrFrameLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        selectedFilter = "";
                        retriveFavoriteList();
                    }
                }, 500);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mLoadMoreListener.loadComplete();
                    }
                },2000);
            }
        });

        rvHome.addOnScrollListener(mLoadMoreListener);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mPtrFrameLayout.autoRefresh(true,400);
            }
        },1000);

    }



    public class MyRecyclerViewAdapter extends BaseWrapperRecyclerAdapter<ChannelListObject.ChannelsBean,MyRecyclerViewAdapter.DataObjectHolder> {


        public class DataObjectHolder extends RecyclerView.ViewHolder
                {
            @BindView(R.id.tvName)
            SnTextView tvName;

            @BindView(R.id.imgFavorite)
            ImageView imgFavorite;

            public DataObjectHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }

        public MyRecyclerViewAdapter(List<ChannelListObject.ChannelsBean> myDataset) {
           appendToList(myDataset);
        }

        @Override
        public DataObjectHolder onCreateItemViewHolder(ViewGroup parent,
                                                   int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.activity_channel_list_item, parent, false);

            DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
            return dataObjectHolder;
        }

        @Override
        public void onBindItemViewHolder(final DataObjectHolder holder, int position) {
            final ChannelListObject.ChannelsBean channelsBean = getItem(position);
            holder.tvName.setText(channelsBean.getChannelId()+" "+channelsBean.getChannelTitle());
            if(channelsBeanHashMap.containsKey(channelsBean.getChannelId())){
                holder.imgFavorite.setColorFilter(ContextCompat.getColor(holder.tvName.getContext(), R.color.colorAccent));
            }else{
                holder.imgFavorite.setColorFilter(ContextCompat.getColor(holder.tvName.getContext(), R.color.textHintColor));
            }
            holder.imgFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                      removeFromFavorite(channelsBean);
                }
            });
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //addToFavorite(channelsBean);
                }
            });
        }
    }


    public void retriveFavoriteList(){

        channelsBeanHashMap.clear();
        String userId = mAuth.getCurrentUser().getUid();
        DatabaseReference myRef = database.getReference("favourite").child(userId).child("list");

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                List<ChannelListObject.ChannelsBean> channelsBeanList = new ArrayList<ChannelListObject.ChannelsBean>();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                    ChannelListObject.ChannelsBean channelsBean = postSnapshot.getValue(ChannelListObject.ChannelsBean.class);
                    channelsBeanHashMap.put(channelsBean.getChannelId(),channelsBean);
                    channelsBeanList.add(channelsBean);
                }
                myRecyclerViewAdapter.clear();
                myRecyclerViewAdapter.appendToList(channelsBeanList);
                myRecyclerViewAdapter.hideFooterView();
                mLoadMoreListener.loadComplete();
                mPtrFrameLayout.refreshComplete();

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                hideDialog();
                showToast("Oops " + databaseError.getMessage());
            }
        });
    }

    public void removeFromFavorite(final ChannelListObject.ChannelsBean channelsBean){
        DatabaseReference myRef = database.getReference("favourite");
        String userId = mAuth.getCurrentUser().getUid();

        Query deleteQuery = myRef.child(userId).child("list").orderByChild("channelId").equalTo(channelsBean.getChannelId());
        deleteQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                isValueUpdated = true;
                dataSnapshot.getRef().removeValue();
                showToast("Favourite removed");
                channelsBeanHashMap.remove(channelsBean.getChannelId());
                myRecyclerViewAdapter.remove(channelsBean,true);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("update", isValueUpdated);
        setResult(RESULT_OK, intent);
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
