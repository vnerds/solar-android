package com.vnerds.solarpanel.objects;

import java.util.List;

public class StateListObject extends CommonResponseObject {

    private String total;
    private List<OemBean> oem;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<OemBean> getOem() {
        return oem;
    }

    public void setOem(List<OemBean> oem) {
        this.oem = oem;
    }

    public static class OemBean {
        private int start_num;
        private String state_id;
        private String name;
        private String oem;

        public int getStart_num() {
            return start_num;
        }

        public void setStart_num(int start_num) {
            this.start_num = start_num;
        }

        public String getState_id() {
            return state_id;
        }

        public void setState_id(String state_id) {
            this.state_id = state_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOem() {
            return oem;
        }

        public void setOem(String oem) {
            this.oem = oem;
        }
    }
}
