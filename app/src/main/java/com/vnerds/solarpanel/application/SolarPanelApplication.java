package com.vnerds.solarpanel.application;

import android.app.Application;


import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.FirebaseApp;
import com.vnerds.solarpanel.apis.APIClass;
import com.vnerds.solarpanel.apis.APIServices;
import com.vnerds.library.Constant;


import retrofit2.Retrofit;

public class SolarPanelApplication extends Application {
    private static Retrofit retrofit;
    static SolarPanelApplication mInstance;
    static APIServices apiServices;
    @Override
    public void onCreate() {
        super.onCreate();
    //   Fabric.with(this, new Crashlytics());
        retrofit =  Constant.getRetrofitClient(APIClass.BASE_URL,true);
        apiServices = retrofit.create(APIServices.class);
        mInstance = this;

        FirebaseApp.initializeApp(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

    }
    public static  APIServices getApiServices() {
        return apiServices;
    }
}
