package com.vnerds.solarpanel.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vnerds.custom.SnTextView;
import com.vnerds.solarpanel.R;
import com.vnerds.solarpanel.activity.BaseActivity;
import com.vnerds.solarpanel.apis.APIClass;
import com.vnerds.solarpanel.application.SolarPanelApplication;
import com.vnerds.solarpanel.objects.DeviceListObject;
import com.vnerds.solarpanel.objects.Global;
import com.vnerds.solarpanel.objects.LoginObject;
import com.vnerds.solarpanel.objects.WorkingInfoObject;
import com.vnerds.solarpanel.objects.WorkingInfo_InvInfo;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class WorkingInfoFragment extends Fragment {

    @BindView(R.id.tvTotalInvertor)
    SnTextView tvTotalInvertor;


  @BindView(R.id.tvRunningInvertor)
    SnTextView tvRunningInvertor;


  @BindView(R.id.tvToday)
    SnTextView tvToday;


  @BindView(R.id.tvYesterday)
    SnTextView tvYesterday;


  @BindView(R.id.tvMonthly)
    SnTextView tvMonthly;


  @BindView(R.id.tvYearly)
    SnTextView tvYearly;


  @BindView(R.id.tvTotal)
    SnTextView tvTotal;


  @BindView(R.id.tvPowerNow)
    SnTextView tvPowerNow;


  @BindView(R.id.tvPlantCapacity)
    SnTextView tvPlantCapacity;




    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_workinginfo, container, false);
    }


    LoginObject login_object;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(false);

        login_object = BaseActivity.getUserObject();
        getAllDevices();
        getInvDevices();

    }

    public void getAllDevices() {
        if (BaseActivity.isNetworkAvailable()) {
            BaseActivity.showDialog();
            Call<WorkingInfoObject> call = SolarPanelApplication.getApiServices().getWorkingInfo(Global.plantBean.getPlant_id(),
                    login_object.getToken(), APIClass.WORKING_INFO);
            call.enqueue(new Callback<WorkingInfoObject>() {
                @Override
                public void onResponse(Call<WorkingInfoObject> call, Response<WorkingInfoObject> response) {
                    BaseActivity.hideDialog();
                    final WorkingInfoObject oemListObject = response.body();
                    if (BaseActivity.isValidResponse(oemListObject)) {
                        WorkingInfoObject.PlantBean.WorkingInfoBean obj = oemListObject.getPlant().getWorking_info();

                    //    tvTotalInvertor.setText("4");
                   //     tvRunningInvertor.setText("4");//need to call api
                        tvToday.setText(obj.getToday_Energy()+" KWh");
                        tvYesterday.setText(obj.getYesterday_energy()+" KWh");
                        tvMonthly.setText(obj.getMonthly_Energy()+" KWh");
                        tvYearly.setText(obj.getYearly_Energy()+" KWh");
                        tvTotal.setText(obj.getTotal_energy()+" KWh");
                        tvPowerNow.setText(obj.getPower_now()+" KWh");
                        tvPlantCapacity.setText(oemListObject.getPlant().getCapacity()+" KWh");

                    } else if (response.errorBody() != null) {
                        try {
                            BaseActivity.showErrorMsg(response.errorBody().string(), response.code());
                        } catch (Exception e) {

                        }
                    }

                }

                @Override
                public void onFailure(Call<WorkingInfoObject> call, Throwable t) {
                    BaseActivity.hideDialog();
                    BaseActivity.showToast(t.getMessage());
                }
            });

        }
    }
  public void getInvDevices() {
        if (BaseActivity.isNetworkAvailable()) {
            BaseActivity.showDialog();
            Call<WorkingInfo_InvInfo> call = SolarPanelApplication.getApiServices().getWorkingInvInfo(Global.plantBean.getPlant_id(),
                    login_object.getToken(), APIClass.INV_INFO);
            call.enqueue(new Callback<WorkingInfo_InvInfo>() {
                @Override
                public void onResponse(Call<WorkingInfo_InvInfo> call, Response<WorkingInfo_InvInfo> response) {
                    BaseActivity.hideDialog();
                    final WorkingInfo_InvInfo oemListObject = response.body();
                    if (BaseActivity.isValidResponse(oemListObject)) {
                        WorkingInfo_InvInfo.PlantBean.InvInfoBean obj = oemListObject.getPlant().getInv_info();

                        tvTotalInvertor.setText(obj.getTotal_device()+"");
                        tvRunningInvertor.setText(obj.getTotal_running_device()+"");//need to call api


                    } else if (response.errorBody() != null) {
                        try {
                            BaseActivity.showErrorMsg(response.errorBody().string(), response.code());
                        } catch (Exception e) {

                        }
                    }

                }

                @Override
                public void onFailure(Call<WorkingInfo_InvInfo> call, Throwable t) {
                    BaseActivity.hideDialog();
                    BaseActivity.showToast(t.getMessage());
                }
            });

        }
    }

    @Override
    public void onResume() {
        super.onResume();


    }


    public Context getApplicationContext() {
        return getActivity();
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onPause() {

        super.onPause();
    }


}
