package com.vnerds.solarpanel.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.vnerds.custom.SnEditText;
import com.vnerds.custom.SnTextView;

import com.vnerds.library.Constant;
import com.vnerds.solarpanel.R;
import com.vnerds.solarpanel.apis.APIClass;
import com.vnerds.solarpanel.application.SolarPanelApplication;

import com.vnerds.solarpanel.objects.Global;
import com.vnerds.solarpanel.objects.OEMListObject;

import java.util.ArrayList;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import butterknife.OnClick;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OEMActivity extends BaseActivity {

    @BindView(R.id.etSearchText)
    SnEditText etSearchText;

    @OnClick(R.id.btnSearch)
    public void onClickSearch(View view) {
        etSearchText.setText("");
        myRecyclerViewAdapter.update(oemBeanListMain);
        Constant.hideKeyboard(this);
    }

    @BindView(R.id.rvHome)
    RecyclerView rvHome;

    MyRecyclerViewAdapter myRecyclerViewAdapter;

    LinearLayoutManager llm;
    List<OEMListObject.OemBean> oemBeanListMain;
    Activity mActivity ;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oem);

        mActivity = this ;
        ButterKnife.bind(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.show();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(getString(R.string.oemlist));

        etSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (isEditTextValid(etSearchText, "Please enter text for search")) {
                        callSearchOEMAPI();
                    }
                    return true;
                }
                return false;
            }
        });

        etSearchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    //  myRecyclerViewAdapter.update(searchText());
                } else {
                    myRecyclerViewAdapter.update(oemBeanListMain);
                }

            }
        });


        oemBeanListMain = new ArrayList<>();
        myRecyclerViewAdapter = new MyRecyclerViewAdapter(oemBeanListMain);

        rvHome.setAdapter(myRecyclerViewAdapter);
        rvHome.setNestedScrollingEnabled(false);
        llm = new LinearLayoutManager(this);
        rvHome.setLayoutManager(llm);


        callOEMAPI();
        //  showOverLay();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Constant.isOEMAdded) {
            Constant.isOEMAdded = false;
            callOEMAPI();
        }
    }

    public ArrayList<OEMListObject.OemBean> searchText() {
        String searchText = getEditText(etSearchText);
        ArrayList<OEMListObject.OemBean> searchedArrayList = new ArrayList<>();
        List<OEMListObject.OemBean> arrayList = myRecyclerViewAdapter.getListView();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            OEMListObject.OemBean obj = arrayList.get(i);
            if (obj.getName().toLowerCase().contains(searchText) || obj.getUname().toLowerCase().contains(searchText)) {
                searchedArrayList.add(obj);
            }
        }
        return searchedArrayList;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_add:
                Constant.callActivity(this, AddOEMActivity.class, null, false);
                break;
            case R.id.action_logout:
                logOutAlert();
                break;

            default:
                break;
        }

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add, menu);
        return true;
    }

    public void callSearchOEMAPI() {
        if (isNetworkAvailable()) {
            BaseActivity.showDialog();
            Call<OEMListObject> call = SolarPanelApplication.getApiServices().getOEMSearchList(1,
                    1000, getUserObject().getToken(), APIClass.LIST, getEditText(etSearchText));
            call.enqueue(new Callback<OEMListObject>() {
                @Override
                public void onResponse(Call<OEMListObject> call, Response<OEMListObject> response) {
                    BaseActivity.hideDialog();
                    final OEMListObject oemListObject = response.body();
                    if (isValidResponse(oemListObject)) {
                        List<OEMListObject.OemBean> oemBeanListMain = oemListObject.getOem();
                        myRecyclerViewAdapter.update(oemBeanListMain);
                    } else if (response.errorBody() != null) {
                        try {
                            showErrorMsg(response.errorBody().string(), response.code());
                        } catch (Exception e) {

                        }
                    }

                }

                @Override
                public void onFailure(Call<OEMListObject> call, Throwable t) {
                    hideDialog();
                    showToast(t.getMessage());
                }
            });

        }
    }

    public void callOEMAPI() {
        if (isNetworkAvailable()) {
            BaseActivity.showDialog();
            Call<OEMListObject> call = SolarPanelApplication.getApiServices().getOEMList(1, 1000, getUserObject().getToken(), APIClass.LIST);
            call.enqueue(new Callback<OEMListObject>() {
                @Override
                public void onResponse(Call<OEMListObject> call, Response<OEMListObject> response) {
                    BaseActivity.hideDialog();
                    final OEMListObject oemListObject = response.body();
                    if (isValidResponse(oemListObject)) {
                        oemBeanListMain = oemListObject.getOem();
                        myRecyclerViewAdapter.update(oemBeanListMain);
                    } else if (response.errorBody() != null) {
                        try {
                            showErrorMsg(response.errorBody().string(), response.code());
                        } catch (Exception e) {

                        }
                    }

                }

                @Override
                public void onFailure(Call<OEMListObject> call, Throwable t) {
                    hideDialog();
                    showToast(t.getMessage());
                }
            });

        }
    }

    public void callDeleteAPI(String oemID) {
        if (isNetworkAvailable()) {
            showDialog();
            Call<OEMListObject> call = SolarPanelApplication.getApiServices().deleteOEM(APIClass.DELETE, oemID, getUserObject().getToken());
            call.enqueue(new Callback<OEMListObject>() {
                @Override
                public void onResponse(Call<OEMListObject> call, Response<OEMListObject> response) {
                    hideDialog();
                    final OEMListObject oemListObject = response.body();
                    if (isValidResponse(oemListObject)) {
                        callOEMAPI();
                        showToast(oemListObject.getSuccess_msg());
                    } else if (response.errorBody() != null) {
                        try {
                            showErrorMsg(response.errorBody().string(), response.code());
                        } catch (Exception e) {

                        }
                    }

                }


                @Override
                public void onFailure(Call<OEMListObject> call, Throwable t) {
                    hideDialog();
                    showToast(t.getMessage());
                }
            });

        }
    }


    public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.DataObjectHolder> {

        private List<OEMListObject.OemBean> mDataset;


        public class DataObjectHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.tvOemName)
            SnTextView tvOemName;

            @BindView(R.id.tvUserName)
            SnTextView tvUserName;

            @BindView(R.id.imgEdit)
            ImageView imgEdit;

            @BindView(R.id.imgDelete)
            ImageView imgDelete;

            public DataObjectHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

        }


        public List<OEMListObject.OemBean> getListView() {
            return mDataset;
        }

        public OEMListObject.OemBean getSelectedItem(int position) {
            return mDataset.get(position);
        }

        public MyRecyclerViewAdapter(List<OEMListObject.OemBean> myDataset) {
            mDataset = myDataset;
        }

        public void update(List<OEMListObject.OemBean> myDataset) {
            mDataset = myDataset;
            notifyDataSetChanged();
        }

        @Override
        public MyRecyclerViewAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                         int viewType) {
            View view;
            {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.activity_oem_list_item, parent, false);
            }

            MyRecyclerViewAdapter.DataObjectHolder dataObjectHolder = new MyRecyclerViewAdapter.DataObjectHolder(view);
            return dataObjectHolder;
        }

        @Override
        public void onBindViewHolder(MyRecyclerViewAdapter.DataObjectHolder holder, int position) {

            final OEMListObject.OemBean rowBean = mDataset.get(position);
            holder.tvOemName.setText(rowBean.getName());
            holder.tvUserName.setText(rowBean.getUname());

            holder.imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(OEMActivity.this);
                    alertDialogBuilder.setTitle(mActivityBase.getString(R.string.app_name));
                    alertDialogBuilder
                            .setMessage(R.string.wouldyouliketodelete)
                            .setCancelable(false)
                            .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    callDeleteAPI(rowBean.getOem_id() + "");
                                }
                            })
                            .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            })
                    ;
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            });
            holder.imgEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Global.oemBean = rowBean;
                    Constant.callActivity(mActivity,AddOEMActivity.class,null,false);

                }
            });
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Global.oemBean = rowBean;
                    Constant.callActivity(mActivity,StateActivity.class,null,false);

                }
            });
        }

        public void addItem(OEMListObject.OemBean dataObj, int index) {
            mDataset.add(dataObj);
            notifyItemInserted(index);

        }

        public void deleteItem(int index) {
            mDataset.remove(index);
            notifyItemRemoved(index);
        }

        @Override
        public int getItemCount() {
            return mDataset.size();
        }
    }


}
