package com.vnerds.solarpanel.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vnerds.custom.SnEditText;
import com.vnerds.custom.SnTextView;
import com.vnerds.library.Constant;
import com.vnerds.solarpanel.R;
import com.vnerds.solarpanel.apis.APIClass;
import com.vnerds.solarpanel.application.SolarPanelApplication;
import com.vnerds.solarpanel.objects.Global;
import com.vnerds.solarpanel.objects.StateListObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class StateActivity extends BaseActivity {

    @BindView(R.id.etSearchText)
    SnEditText etSearchText;

    @OnClick(R.id.btnSearch)
    public void onClickSearch(View view) {
        etSearchText.setText("");
        myRecyclerViewAdapter.update(oemBeanListMain);
        Constant.hideKeyboard(this);
    }

    @BindView(R.id.rvHome)
    RecyclerView rvHome;

    @BindView(R.id.linSearch)
    LinearLayout linSearch;

    MyRecyclerViewAdapter myRecyclerViewAdapter;

    LinearLayoutManager llm;
    List<StateListObject.OemBean> oemBeanListMain;
    Activity mActivity;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oem);

        mActivity = this;
        ButterKnife.bind(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.show();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(getString(R.string.state));
        linSearch.setVisibility(View.GONE);
        etSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (isEditTextValid(etSearchText, "Please enter text for search")) {
                        callSearchOEMAPI();
                    }
                    return true;
                }
                return false;
            }
        });

        etSearchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    //  myRecyclerViewAdapter.update(searchText());
                } else {
                    myRecyclerViewAdapter.update(oemBeanListMain);
                }

            }
        });


        oemBeanListMain = new ArrayList<>();
        myRecyclerViewAdapter = new MyRecyclerViewAdapter(oemBeanListMain);

        rvHome.setAdapter(myRecyclerViewAdapter);
        rvHome.setNestedScrollingEnabled(false);
        llm = new LinearLayoutManager(this);
        rvHome.setLayoutManager(llm);


        callOEMAPI();


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Constant.isOEMAdded) {
            Constant.isOEMAdded = false;
            callOEMAPI();
        }
    }

    public ArrayList<StateListObject.OemBean> searchText() {
        String searchText = getEditText(etSearchText);
        ArrayList<StateListObject.OemBean> searchedArrayList = new ArrayList<>();
        List<StateListObject.OemBean> arrayList = myRecyclerViewAdapter.getListView();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            StateListObject.OemBean obj = arrayList.get(i);
            if (obj.getName().toLowerCase().contains(searchText) || obj.getOem().toLowerCase().contains(searchText)) {
                searchedArrayList.add(obj);
            }
        }
        return searchedArrayList;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                Constant.callActivity(this, AddStateActivity.class, null, false);
                break;
            case R.id.action_logout:
                logOutAlert();
                break;
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add, menu);
        MenuItem item = menu.findItem(R.id.action_logout);
        item.setVisible(false);
        return true;
    }

    public void callSearchOEMAPI() {
        if (isNetworkAvailable()) {


        }
    }

    public void callOEMAPI() {
        if (isNetworkAvailable()) {
            BaseActivity.showDialog();
            Call<StateListObject> call = SolarPanelApplication.getApiServices().getStateList(1, APIClass.PAGE_COUNT, getUserObject().getToken(), APIClass.LIST, Global.oemBean.getOem_id());
            call.enqueue(new Callback<StateListObject>() {
                @Override
                public void onResponse(Call<StateListObject> call, Response<StateListObject> response) {
                    BaseActivity.hideDialog();
                    final StateListObject StateListObject = response.body();
                    if (isValidResponse(StateListObject)) {
                        oemBeanListMain = StateListObject.getOem();
                        myRecyclerViewAdapter.update(oemBeanListMain);
                    } else if (response.errorBody() != null) {
                        try {
                            showErrorMsg(response.errorBody().string(), response.code());
                        } catch (Exception e) {

                        }
                    }

                }

                @Override
                public void onFailure(Call<StateListObject> call, Throwable t) {
                    hideDialog();
                    showToast(t.getMessage());
                }
            });

        }
    }

    public void callDeleteAPI(String oemID) {
        if (isNetworkAvailable()) {
            showDialog();
            Call<StateListObject> call = SolarPanelApplication.getApiServices().deleteState(APIClass.DELETE, oemID, getUserObject().getToken());
            call.enqueue(new Callback<StateListObject>() {
                @Override
                public void onResponse(Call<StateListObject> call, Response<StateListObject> response) {
                    hideDialog();
                    final StateListObject StateListObject = response.body();
                    if (isValidResponse(StateListObject)) {
                        callOEMAPI();
                        showToast(StateListObject.getSuccess_msg());
                    } else if (response.errorBody() != null) {
                        try {
                            showErrorMsg(response.errorBody().string(), response.code());
                        } catch (Exception e) {

                        }
                    }

                }


                @Override
                public void onFailure(Call<StateListObject> call, Throwable t) {
                    hideDialog();
                    showToast(t.getMessage());
                }
            });

        }
    }


    public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.DataObjectHolder> {

        private List<StateListObject.OemBean> mDataset;


        public class DataObjectHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.tvOemName)
            SnTextView tvOemName;

            @BindView(R.id.tvUserName)
            SnTextView tvUserName;

            @BindView(R.id.tvLabelUserName)
            SnTextView tvLabelUserName;

            @BindView(R.id.tvLabelOemName)
            SnTextView tvLabelOemName;

            @BindView(R.id.imgEdit)
            ImageView imgEdit;

            @BindView(R.id.imgDelete)
            ImageView imgDelete;

            public DataObjectHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

        }


        public List<StateListObject.OemBean> getListView() {
            return mDataset;
        }

        public StateListObject.OemBean getSelectedItem(int position) {
            return mDataset.get(position);
        }

        public MyRecyclerViewAdapter(List<StateListObject.OemBean> myDataset) {
            mDataset = myDataset;
        }

        public void update(List<StateListObject.OemBean> myDataset) {
            mDataset = myDataset;
            notifyDataSetChanged();
        }

        @Override
        public MyRecyclerViewAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                         int viewType) {
            View view;
            {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.activity_oem_list_item, parent, false);
            }

            MyRecyclerViewAdapter.DataObjectHolder dataObjectHolder = new MyRecyclerViewAdapter.DataObjectHolder(view);
            return dataObjectHolder;
        }

        @Override
        public void onBindViewHolder(MyRecyclerViewAdapter.DataObjectHolder holder, int position) {

            final StateListObject.OemBean rowBean = mDataset.get(position);
            holder.tvOemName.setText(rowBean.getName());
            holder.tvUserName.setText(rowBean.getOem());
            holder.tvLabelUserName.setText("OEM: ");
            holder.tvLabelOemName.setText("Name: ");

            holder.imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(StateActivity.this);
                    alertDialogBuilder.setTitle(mActivityBase.getString(R.string.app_name));
                    alertDialogBuilder
                            .setMessage(R.string.wouldyouliketodelete)
                            .setCancelable(false)
                            .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    callDeleteAPI(rowBean.getState_id() + "");
                                }
                            })
                            .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            })
                    ;
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            });
            holder.imgEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Global.stateBean = rowBean;
                    Constant.callActivity(mActivity, AddStateActivity.class, null, false);

                }
            });
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Global.stateBean = rowBean;
                    Constant.callActivity(mActivity, DepartmentActivity.class, null, false);
                }
            });
        }

        public void addItem(StateListObject.OemBean dataObj, int index) {
            mDataset.add(dataObj);
            notifyItemInserted(index);

        }

        public void deleteItem(int index) {
            mDataset.remove(index);
            notifyItemRemoved(index);
        }

        @Override
        public int getItemCount() {
            return mDataset.size();
        }
    }


}
