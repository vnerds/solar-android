package com.vnerds.solarpanel.objects;

import com.vnerds.solarpanel.activity.StateActivity;

public class Global {
   public static OEMListObject.OemBean oemBean;
   public static StateListObject.OemBean stateBean;
   public static YearListObject.YearBean yearBean;
   public static DepartmentListObject.DepartmentBean departmentBean;
   public static PlantListObject.PlantBean plantBean;
   public static DeviceListObject.PlantBean.DeviceListBean graphPlantBean;
}
