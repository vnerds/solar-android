package com.vnerds.solarpanel.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;

import com.google.firebase.iid.FirebaseInstanceId;
import com.vnerds.custom.SharedPrefrenceUtil;
import com.vnerds.solarpanel.R;
import com.vnerds.library.Constant;
import com.vnerds.solarpanel.apis.APIClass;
import com.vnerds.solarpanel.objects.LoginObject;


public class SplashActivity extends BaseActivity {
    Handler handler;
    Runnable runnable;
    int SPLASH_TIME = 2000;//Spash time

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();
        Constant.changeStatusBarColor(this, Color.TRANSPARENT);
        APIClass.GCM_TOKEN  = FirebaseInstanceId.getInstance().getToken();
        if(APIClass.GCM_TOKEN == null ){
            APIClass.GCM_TOKEN = "12345";
        }else if(APIClass.GCM_TOKEN !=null && APIClass.GCM_TOKEN.length()==0){
            APIClass.GCM_TOKEN = "12345";
        }

        final LoginObject loginObject = getUserObject();

        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if (loginObject != null && loginObject.getToken()!=null && loginObject.getToken().length() > 0) {
                    Constant.callActivity(SplashActivity.this, OEMActivity.class, null, true);
                } else {
                    Constant.callActivity(SplashActivity.this, LoginActivity.class, null, true);
                }
            }
        };


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isNetworkAvailable()) {
            handler.postDelayed(runnable, SPLASH_TIME);
        }
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (runnable != null) {
            handler.removeCallbacks(runnable);
        }
        finish();
    }


}
