package com.vnerds.solarpanel.apis;

/**
 * Created by parag on 8/22/2017.
 */

public class APIClass {
  //  public final static String BASE_IP ="http://www.wcst.co.in/";
    public final static String BASE_IP ="http://117.247.80.164:85/";
    public final static String BASE_URL = BASE_IP+"herapi/";


    public final static String login = BASE_URL +"login.php";
    public final static String oemList = BASE_URL +"oem.php";
    public final static String stateList = BASE_URL +"state.php";
    public final static String yearList = BASE_URL +"year.php";
    public final static String plantList = BASE_URL +"plant.php";
    public final static String plantView = BASE_URL +"plant_view.php";
    public final static String plantDevice = BASE_URL +"plant_device.php";
    public final static String departmentList = BASE_URL +"department.php";
    public final static String plantInvGraph = BASE_URL +"plant_inv_graph.php";
    //public final static String addOEMList = BASE_URL +"oem.php?tag=add";
    public static final String getCountryCode = "http://ip-api.com/json";


    //1 = Main User, 2 = OEM, 3 = Department, 4 = Plant

    public static final int USER_MAIN = 1;
    public static final int USER_OEM = 2;
    public static final int USER_DEPARTMENT = 3;
    public static final int USER_PLANT = 4;

    //Constant parameters
    public static final String USERNAME = "uname";
    public static final String PASS = "pass";
    public static final String PASSWORD = "password";
    public static final String TOKEN = "token";
    public static final String OEM_ID = "oem_id";
    public static final String DEPARTMENT_ID = "department_id";
    public static final String YEAR = "year";
    public static final String TAG = "tag";
    public static final String CITY = "city";
    public static final String ADD = "add";
    public static final String EDIT = "edit";
    public static final String DELETE = "delete";
    public static final String VIEW = "view";
    public static final String LIST = "list";
    public static final String DEVICE_LIST = "device_list";
    public static final String SAVING_INFO = "saving_info";
    public static final String WORKING_INFO = "working_info";
    public static final String INV_INFO = "inv_info";
    public static final String PAGE_NUMBER = "page_num";
    public static final String LIMIT = "limit";
    public static final String GRAPH = "graph";
    public static final String NAME = "name";
    public static final String ADDRESS = "address";
    public static final String FILTER_OEM = "filter_oem";
    public static final String FILTER_PLANT = "filter_plant";
    public static final String STATE_ID = "state_id";
    public static final int PAGE_COUNT = 1000 ;
  public static final String PLANT_ID =  "plant_id";
  public static final String KEY =  "key";
  public static final String VAL =  "val";
  public static final String CHART_TYPE =  "chart_type";
    public static final String ID = "id";


    //static variable for store local value
    public static  String GCM_TOKEN = "";

    //For shared preferance
    public static final String SHARED_LOGIN =  "Login";
}
