package com.vnerds.solarpanel.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;

import com.vnerds.custom.SnEditText;
import com.vnerds.custom.SnTextView;
import com.vnerds.library.Constant;
import com.vnerds.solarpanel.R;
import com.vnerds.solarpanel.apis.APIClass;
import com.vnerds.solarpanel.apis.APIServices;
import com.vnerds.solarpanel.application.SolarPanelApplication;
import com.vnerds.solarpanel.objects.DepartmentListObject;
import com.vnerds.solarpanel.objects.Global;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DepartmentActivity extends BaseActivity {

    @BindView(R.id.etSearchText)
    SnEditText etSearchText;

    @OnClick(R.id.btnSearch)
    public void onClickSearch(View view) {
        etSearchText.setText("");
        myRecyclerViewAdapter.update(oemBeanListMain);
        Constant.hideKeyboard(this);
    }

    @BindView(R.id.rvHome)
    RecyclerView rvHome;

    MyRecyclerViewAdapter myRecyclerViewAdapter;

    LinearLayoutManager llm;

    List<DepartmentListObject.DepartmentBean> oemBeanListMain;
    Activity mActivity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oem);
        mActivity = this;
        ButterKnife.bind(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.show();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(getString(R.string.departmentlist));

        etSearchText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (isEditTextValid(etSearchText, "Please enter text for search")) {
                        callSearchOEMAPI();
                    }
                    return true;
                }
                return false;
            }
        });

        etSearchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    //  myRecyclerViewAdapter.update(searchText());
                } else {
                    myRecyclerViewAdapter.update(oemBeanListMain);
                }

            }
        });
       /* imgLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(OEMActivity.this);
                alertDialogBuilder.setTitle(mActivityBase.getString(R.string.app_name));
                alertDialogBuilder
                        .setMessage(R.string.wouldyouliketologout)
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mAuth.signOut();
                                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                                LoginManager.getInstance().logOut();
                                SharedPrefrenceUtil.setString(getApplicationContext(), getString(R.string.token),null);
                                Constant.callActivity(OEMActivity.this,LoginActivity.class,null,true);
                            }
                        })
                        .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        })
                ;
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });*/


        oemBeanListMain = new ArrayList<>();
        myRecyclerViewAdapter = new MyRecyclerViewAdapter(oemBeanListMain);

        rvHome.setAdapter(myRecyclerViewAdapter);
        rvHome.setNestedScrollingEnabled(false);
        llm = new LinearLayoutManager(this);
        rvHome.setLayoutManager(llm);


        callOEMAPI();
        //  showOverLay();

    }

    public ArrayList<DepartmentListObject.DepartmentBean> searchText() {
        String searchText = getEditText(etSearchText);
        ArrayList<DepartmentListObject.DepartmentBean> searchedArrayList = new ArrayList<>();
        List<DepartmentListObject.DepartmentBean> arrayList = myRecyclerViewAdapter.getListView();
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            DepartmentListObject.DepartmentBean obj = arrayList.get(i);
            if (obj.getName().toLowerCase().contains(searchText) || obj.getUname().toLowerCase().contains(searchText)) {
                searchedArrayList.add(obj);
            }
        }
        return searchedArrayList;
    }
    /*public void showOverLay(){
        favouriteBuilder =  showOverLayWithCallBack(this,imgFav,favouriteText, FocusGravity.CENTER, ShapeType.CIRCLE);;
        favouriteBuilder.setListener(new MaterialIntroListener() {
            @Override
            public void onUserClicked(String s) {
                if(isProfileSee) {
                    isProfileSee = false;
                    sortBuilder.show();
                }
            }
        });

        sortBuilder = showOverLayWithCallBack(this,imgSort,sortText, FocusGravity.CENTER, ShapeType.CIRCLE);
        sortBuilder.setListener(new MaterialIntroListener() {
            @Override
            public void onUserClicked(String s) {

            }
        });

        favouriteBuilder.show();
    }*/


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_add:
                Constant.callActivity(this, AddDepartmentActivity.class, null, false);
                break;
            case R.id.action_logout:
                logOutAlert();
                break;
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add, menu);
        MenuItem item = menu.findItem(R.id.action_logout);
        item.setVisible(false);
        return true;
    }

    public void callSearchOEMAPI() {
        if (isNetworkAvailable()) {
            BaseActivity.showDialog();
            Call<DepartmentListObject> call = SolarPanelApplication.getApiServices().getDepartmentSearchList(1,
                    APIClass.PAGE_COUNT, getUserObject().getToken(), APIClass.LIST, getEditText(etSearchText), Global.oemBean.getOem_id(), Global.stateBean.getState_id());
            call.enqueue(new Callback<DepartmentListObject>() {
                @Override
                public void onResponse(Call<DepartmentListObject> call, Response<DepartmentListObject> response) {
                    BaseActivity.hideDialog();
                    final DepartmentListObject oemListObject = response.body();
                    if (isValidResponse(oemListObject)) {
                        List<DepartmentListObject.DepartmentBean> oemBeanListMain = oemListObject.getDepartment();
                        myRecyclerViewAdapter.update(oemBeanListMain);
                    } else if (response.errorBody() != null) {
                        try {
                            showErrorMsg(response.errorBody().string(), response.code());
                        } catch (Exception e) {

                        }
                    }

                }

                @Override
                public void onFailure(Call<DepartmentListObject> call, Throwable t) {
                    hideDialog();
                    showToast(t.getMessage());
                }
            });

        }
    }

    public void callOEMAPI() {
        if (isNetworkAvailable()) {
            BaseActivity.showDialog();
            Call<DepartmentListObject> call = SolarPanelApplication.getApiServices().getDepartmentList(
                    1, APIClass.PAGE_COUNT, getUserObject().getToken(), APIClass.LIST, Global.oemBean.getOem_id(), Global.stateBean.getState_id());
            call.enqueue(new Callback<DepartmentListObject>() {
                @Override
                public void onResponse(Call<DepartmentListObject> call, Response<DepartmentListObject> response) {
                    BaseActivity.hideDialog();
                    final DepartmentListObject oemListObject = response.body();
                    if (isValidResponse(oemListObject)) {
                        oemBeanListMain = oemListObject.getDepartment();
                        myRecyclerViewAdapter.update(oemBeanListMain);
                    } else if (response.errorBody() != null) {
                        try {
                            showErrorMsg(response.errorBody().string(), response.code());
                        } catch (Exception e) {

                        }
                    }

                }

                @Override
                public void onFailure(Call<DepartmentListObject> call, Throwable t) {
                    hideDialog();
                    showToast(t.getMessage());
                }
            });

        }
    }

    public void callDeleteAPI(String oemID) {
        if (isNetworkAvailable()) {
            BaseActivity.showDialog();
            Call<DepartmentListObject> call = SolarPanelApplication.getApiServices().deleteDepartment(APIClass.DELETE, oemID);
            call.enqueue(new Callback<DepartmentListObject>() {
                @Override
                public void onResponse(Call<DepartmentListObject> call, Response<DepartmentListObject> response) {
                    BaseActivity.hideDialog();
                    final DepartmentListObject oemListObject = response.body();
                    if (isValidResponse(oemListObject)) {
                        callOEMAPI();
                        showToast(oemListObject.getSuccess_msg());
                    } else if (response.errorBody() != null) {
                        try {
                            showErrorMsg(response.errorBody().string(), response.code());
                        } catch (Exception e) {

                        }
                    }

                }


                @Override
                public void onFailure(Call<DepartmentListObject> call, Throwable t) {
                    hideDialog();
                    showToast(t.getMessage());
                }
            });

        }
    }


    public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.DataObjectHolder> {

        private List<DepartmentListObject.DepartmentBean> mDataset;


        public class DataObjectHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.tvDate)
            SnTextView tvDate;
            @BindView(R.id.tvCity)
            SnTextView tvCity;

            @BindView(R.id.tvOemName)
            SnTextView tvOemName;

            @BindView(R.id.tvUserName)
            SnTextView tvUserName;

            @BindView(R.id.imgEdit)
            ImageView imgEdit;

            @BindView(R.id.imgDelete)
            ImageView imgDelete;

            public DataObjectHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

        }


        public List<DepartmentListObject.DepartmentBean> getListView() {
            return mDataset;
        }

        public DepartmentListObject.DepartmentBean getSelectedItem(int position) {
            return mDataset.get(position);
        }

        public MyRecyclerViewAdapter(List<DepartmentListObject.DepartmentBean> myDataset) {
            mDataset = myDataset;
        }

        public void update(List<DepartmentListObject.DepartmentBean> myDataset) {
            mDataset = myDataset;
            notifyDataSetChanged();
        }

        @Override
        public MyRecyclerViewAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
                                                                         int viewType) {
            View view;
            {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.activity_department_list_item, parent, false);
            }

            MyRecyclerViewAdapter.DataObjectHolder dataObjectHolder = new MyRecyclerViewAdapter.DataObjectHolder(view);
            return dataObjectHolder;
        }

        @Override
        public void onBindViewHolder(MyRecyclerViewAdapter.DataObjectHolder holder, int position) {

            final DepartmentListObject.DepartmentBean rowBean = mDataset.get(position);
            holder.tvOemName.setText(rowBean.getName());
            holder.tvUserName.setText(rowBean.getUname());
            holder.tvDate.setText(rowBean.getDate_added());
            holder.tvCity.setText(rowBean.getCity());

            holder.imgDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(DepartmentActivity.this);
                    alertDialogBuilder.setTitle(mActivityBase.getString(R.string.app_name));
                    alertDialogBuilder
                            .setMessage(R.string.wouldyouliketodelete)
                            .setCancelable(false)
                            .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    callDeleteAPI(rowBean.getDepartment_id());
                                }
                            })
                            .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            })
                    ;
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            });
            holder.imgEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Global.departmentBean = rowBean;
                    Constant.callActivity(mActivity, AddDepartmentActivity.class, null, false);
                }
            });

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Global.departmentBean = rowBean;
                    Constant.callActivity(mActivity, YearActivity.class, null, false);
                }
            });
        }

        public void addItem(DepartmentListObject.DepartmentBean dataObj, int index) {
            mDataset.add(dataObj);
            notifyItemInserted(index);

        }

        public void deleteItem(int index) {
            mDataset.remove(index);
            notifyItemRemoved(index);
        }

        @Override
        public int getItemCount() {
            return mDataset.size();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Constant.isOEMAdded) {
            Constant.isOEMAdded = false;
            callOEMAPI();
        }
    }
}
