package com.vnerds.solarpanel.objects;

import java.util.List;

public class GraphObject extends CommonResponseObject {

    private GraphDataBean graph_data;

    public GraphDataBean getGraph_data() {
        return graph_data;
    }

    public void setGraph_data(GraphDataBean graph_data) {
        this.graph_data = graph_data;
    }

    public static class GraphDataBean {
        private InverterInfoDataBean inverter_info_data;
        private List<String> InverterID;

        public InverterInfoDataBean getInverter_info_data() {
            return inverter_info_data;
        }

        public void setInverter_info_data(InverterInfoDataBean inverter_info_data) {
            this.inverter_info_data = inverter_info_data;
        }

        public List<String> getInverterID() {
            return InverterID;
        }

        public void setInverterID(List<String> InverterID) {
            this.InverterID = InverterID;
        }

        public static class InverterInfoDataBean {
            private List<F01175008453Bean> F01175008453;

            public List<F01175008453Bean> getF01175008453() {
                return F01175008453;
            }

            public void setF01175008453(List<F01175008453Bean> F01175008453) {
                this.F01175008453 = F01175008453;
            }

            public static class F01175008453Bean {
                private String Y;
                private String n;
                private String j;
                private String G;
                private String i;
                private String s;
                private String Output_Power;

                public String getY() {
                    return Y;
                }

                public void setY(String Y) {
                    this.Y = Y;
                }

                public String getN() {
                    return n;
                }

                public void setN(String n) {
                    this.n = n;
                }

                public String getJ() {
                    return j;
                }

                public void setJ(String j) {
                    this.j = j;
                }

                public String getG() {
                    return G;
                }

                public void setG(String G) {
                    this.G = G;
                }

                public String getI() {
                    return i;
                }

                public void setI(String i) {
                    this.i = i;
                }

                public String getS() {
                    return s;
                }

                public void setS(String s) {
                    this.s = s;
                }

                public String getOutput_Power() {
                    return Output_Power;
                }

                public void setOutput_Power(String Output_Power) {
                    this.Output_Power = Output_Power;
                }
            }
        }
    }
}
