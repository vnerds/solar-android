package com.vnerds.solarpanel.objects;

import java.util.List;

public class YearListObject extends CommonResponseObject {

    private List<YearBean> year;

    public List<YearBean> getYear() {
        return year;
    }

    public void setYear(List<YearBean> year) {
        this.year = year;
    }

    public static class YearBean {
        private int year;
        private String title;

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
