package com.vnerds.solarpanel.objects;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by parag on 8/22/2017.
 */

public class ChannelListObject extends CommonResponseObject{

    @SerializedName("channels")
    private List<ChannelsBean> channels;

    public List<ChannelsBean> getChannels() {
        return channels;
    }

    public void setChannels(List<ChannelsBean> channels) {
        this.channels = channels;
    }

    public static class ChannelsBean {
        /**
         * channelId : 1
         * channelTitle : HBO
         * channelStbNumber : 411
         */

        @SerializedName("channelId")
        private int channelId;
        @SerializedName("channelTitle")
        private String channelTitle;
        @SerializedName("channelStbNumber")
        private int channelStbNumber;

        public int getChannelId() {
            return channelId;
        }

        public void setChannelId(int channelId) {
            this.channelId = channelId;
        }

        public String getChannelTitle() {
            return channelTitle;
        }

        public void setChannelTitle(String channelTitle) {
            this.channelTitle = channelTitle;
        }

        public int getChannelStbNumber() {
            return channelStbNumber;
        }

        public void setChannelStbNumber(int channelStbNumber) {
            this.channelStbNumber = channelStbNumber;
        }
    }
}
