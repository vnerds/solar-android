package com.vnerds.solarpanel.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.vnerds.custom.SharedPrefrenceUtil;
import com.vnerds.custom.SnEditText;
import com.vnerds.custom.SnTextView;
import com.vnerds.solarpanel.R;
import com.vnerds.solarpanel.apis.APIClass;
import com.vnerds.solarpanel.objects.CommonResponseObject;
import com.vnerds.library.Constant;
import com.vnerds.solarpanel.objects.LoginObject;


public class BaseActivity extends AppCompatActivity {
    static Dialog alertDialog;
    public static AppCompatActivity mActivityBase;

    public static void showErrorMsg(String response, int responseCode) {
        Gson gson = new Gson();
        CommonResponseObject obj = gson.fromJson(response, CommonResponseObject.class);
        showToast(obj.getError_msg());


    }
    public static void logOutAlert() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivityBase);
        alertDialogBuilder.setTitle(mActivityBase.getResources().getString(R.string.app_name));
        alertDialogBuilder
                .setMessage(mActivityBase.getString(R.string.wouldyouliketologout))
                .setCancelable(false)
                .setNegativeButton(mActivityBase.getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .setPositiveButton(mActivityBase.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SharedPrefrenceUtil.setCustomObject(mActivityBase,mActivityBase.getResources().getString(R.string.app_name), APIClass.SHARED_LOGIN, null);
                        Constant.callActivity(mActivityBase,LoginActivity.class,null,true);
                        mActivityBase.finishAffinity();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public static LoginObject getUserObject(){
        return  (LoginObject) SharedPrefrenceUtil.getCustomObject(mActivityBase,mActivityBase.getResources().getString(R.string.app_name), APIClass.SHARED_LOGIN, new LoginObject());
    }
    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    public static boolean isValidResponse(CommonResponseObject commonResponseObject) {
        if (commonResponseObject != null) {
            if (commonResponseObject.getError() == 0) {
                return true;
            } else if(commonResponseObject.getError() == 1){
                if(commonResponseObject.getError_msg().equalsIgnoreCase("Invalid Token!")){
                    SharedPrefrenceUtil.setCustomObject(mActivityBase,mActivityBase.getResources().getString(R.string.app_name), APIClass.SHARED_LOGIN, null);
                    mActivityBase.finishAffinity();
                }
                return false;

            }
        }
        return false;
    }

    public static boolean isValidResponseWithAlert(CommonResponseObject commonResponseObject) {
        if (commonResponseObject != null) {
            if (commonResponseObject.getError() == 0) {
                return true;
            } else {
                showToast(commonResponseObject.getError_msg());
                return false;
            }
        }
        return false;
    }


    public void playAudio(String newVideoPath) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(newVideoPath));
        intent.setDataAndType(Uri.parse(newVideoPath), "audio/*");
        startActivity(intent);
    }

    public static void clearGarbageCollection() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                System.gc();
            }
        }, 3000);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mActivityBase = this;

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityBase = this;

        View child = getLayoutInflater().inflate(R.layout.custom_progress_bar, null);
        SnTextView tvMessage = (SnTextView) child.findViewById(R.id.tvMessage);

        ProgressBar progressBar = (ProgressBar) child.findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(new LightingColorFilter(0xFFfafafa, 0xFFfafafa));

        alertDialog = new Dialog(new ContextThemeWrapper(mActivityBase, R.style.AlertDialogCustom));
        alertDialog.setCancelable(false);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(child);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }


    public static void showDialog() {
        try {
            alertDialog.show();
        } catch (Exception e) {
            View child = mActivityBase.getLayoutInflater().inflate(R.layout.custom_progress_bar, null);
            SnTextView tvMessage = (SnTextView) child.findViewById(R.id.tvMessage);
            ProgressBar progressBar = (ProgressBar) child.findViewById(R.id.progressBar);
            progressBar.getIndeterminateDrawable().setColorFilter(new LightingColorFilter(0xFFFFFFFF, 0xFFFF8161));
            alertDialog = new Dialog(new ContextThemeWrapper(mActivityBase, R.style.AlertDialogCustom));
            alertDialog.setCancelable(false);
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setContentView(child);
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertDialog.show();
        }

    }

    public static void hideDialog() {
        try {
            alertDialog.dismiss();
        } catch (Exception e) {

        }

    }

    public static boolean isNetworkAvailable() {
        if (Constant.isNetworkAvailable(mActivityBase)) {
            return true;
        } else {
            showToast(mActivityBase.getString(R.string.networknotavailable));
            return false;
        }
    }

    public static void showToast(String msg) {
        {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivityBase);
            alertDialogBuilder.setTitle(mActivityBase.getString(R.string.app_name));
            alertDialogBuilder
                    .setMessage(msg)
                    .setCancelable(false)
                    .setPositiveButton(mActivityBase.getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                        }
                    })
            ;
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }
 public static void showToastWithExit(String msg) {
        {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mActivityBase);
            alertDialogBuilder.setTitle(mActivityBase.getString(R.string.app_name));
            alertDialogBuilder
                    .setMessage(msg)
                    .setCancelable(false)
                    .setPositiveButton(mActivityBase.getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            mActivityBase.finish();
                        }
                    })
            ;
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }

    public static boolean isEditTextValid(SnEditText mEditText) {
        if (mEditText != null && mEditText.getText().toString().trim().length() > 0) {
            return true;
        }
        return false;
    }


    public static boolean isStringTextValid(String mText, String errMsg) {
        if (mText != null && mText.trim().length() > 0) {
            return true;
        }
        return false;
    }

    public static boolean isValidMobile(SnEditText phone, String msg) {
        boolean isValid = android.util.Patterns.PHONE.matcher(getEditText(phone)).matches();
        if (isValid) {
            return true;
        }
        showToast(msg);
        return false;

    }

    public static boolean isEditTextValid(SnEditText mEditText, String errMsg) {
        if (mEditText != null && mEditText.getText().toString().trim().length() > 0) {
            return true;
        }
        showToast(errMsg);
        return false;
    }

    public static boolean isTextViewValid(SnTextView mEditText, String errMSG) {
        if (mEditText != null && mEditText.getText().toString().trim().length() > 0) {
            return true;
        }
        return false;
    }

    public static boolean isValidPassWordLength(SnEditText mEditText) {
        String password = getEditText(mEditText);
        int length = password.length();
        if (length >= 4) {
            return true;
        }
        return false;
    }

    public static boolean isValidPassWordLength(SnEditText mEditText, String alertMsg) {
        String password = getEditText(mEditText);
        int length = password.length();
        if (length >= 6) {
            return true;
        }

        return false;
    }

    public static boolean isValidMobileLength(SnEditText mEditText, String msg) {
        String password = getEditText(mEditText);
        // int length = password.length();
        if (isValidMobile(password)) {
            return true;
        }

        return false;
    }

    private static boolean isValidMobile(String phone) {
        return Patterns.PHONE.matcher(phone).matches();
    }

    public static boolean isValidOTPLength(SnEditText mEditText, String msg) {
        String password = getEditText(mEditText);
        int length = password.length();
        if (length == 6) {
            return true;
        }

        return false;
    }

    public static String getEditText(SnEditText mEditText) {
        return mEditText.getText().toString().trim();
    }

    public static String getTextValue(SnTextView mTextView) {
        return mTextView.getText().toString().trim();
    }


}
