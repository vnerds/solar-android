package com.vnerds.solarpanel.objects;

import java.util.List;

public class PlantListObject extends  CommonResponseObject {

    private String total;
    private List<PlantBean> plant;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<PlantBean> getPlant() {
        return plant;
    }

    public void setPlant(List<PlantBean> plant) {
        this.plant = plant;
    }

    public static class PlantBean {
        private int start_num;
        private String plant_id;
        private String name;
        private String uname;
        private String city;
        private String capacity;
        private String address;
        private String inv_name;
        private String solar_module;
        private String install_date;
        private List<String> device_data;

        public int getStart_num() {
            return start_num;
        }

        public void setStart_num(int start_num) {
            this.start_num = start_num;
        }

        public String getPlant_id() {
            return plant_id;
        }

        public void setPlant_id(String plant_id) {
            this.plant_id = plant_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUname() {
            return uname;
        }

        public void setUname(String uname) {
            this.uname = uname;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCapacity() {
            return capacity;
        }

        public void setCapacity(String capacity) {
            this.capacity = capacity;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getInv_name() {
            return inv_name;
        }

        public void setInv_name(String inv_name) {
            this.inv_name = inv_name;
        }

        public String getSolar_module() {
            return solar_module;
        }

        public void setSolar_module(String solar_module) {
            this.solar_module = solar_module;
        }

        public String getInstall_date() {
            return install_date;
        }

        public void setInstall_date(String install_date) {
            this.install_date = install_date;
        }

        public List<String> getDevice_data() {
            return device_data;
        }

        public void setDevice_data(List<String> device_data) {
            this.device_data = device_data;
        }
    }
}
