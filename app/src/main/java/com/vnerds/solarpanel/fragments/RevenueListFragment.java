package com.vnerds.solarpanel.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vnerds.custom.SnTextView;
import com.vnerds.solarpanel.R;
import com.vnerds.solarpanel.activity.BaseActivity;
import com.vnerds.solarpanel.apis.APIClass;
import com.vnerds.solarpanel.application.SolarPanelApplication;
import com.vnerds.solarpanel.objects.DeviceListObject;
import com.vnerds.solarpanel.objects.Global;
import com.vnerds.solarpanel.objects.LoginObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RevenueListFragment extends Fragment {


    @BindView(R.id.rv)
    RecyclerView rv;


    public interface MyClickListener {
        public void onItemClick(int position, View v);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            return inflater.inflate(R.layout.fragment_recyclerview, container, false);
    }

    MyRecyclerViewAdapter myRecyclerViewAdapter;
    LoginObject login_object;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(false);


        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(new GridLayoutManager(getActivity(), 1));
        login_object = BaseActivity.getUserObject();
        // login_object.setEmail("shah.hardik222@gmail.com");
        ArrayList<DeviceListObject.PlantBean.SavingInfoBean> myDataset = new ArrayList<>();
        myRecyclerViewAdapter = new MyRecyclerViewAdapter(myDataset);

        rv.setAdapter(myRecyclerViewAdapter);

        getAllDevices();


    }
    public void getAllDevices() {
        if (BaseActivity.isNetworkAvailable()) {
            BaseActivity.showDialog();
            Call<DeviceListObject> call = SolarPanelApplication.getApiServices().getDeviceList(Global.plantBean.getPlant_id(),
                    login_object.getToken(),   APIClass.SAVING_INFO);
            call.enqueue(new Callback<DeviceListObject>() {
                @Override
                public void onResponse(Call<DeviceListObject> call, Response<DeviceListObject> response) {
                    BaseActivity.hideDialog();
                    final DeviceListObject oemListObject = response.body();
                    if (BaseActivity.isValidResponse(oemListObject)) {
                       // oemBeanListMain = oemListObject.getPlant();
                        List<DeviceListObject.PlantBean.SavingInfoBean> list = new ArrayList<>();
                        list.add(oemListObject.getPlant().getSaving_info());
                        myRecyclerViewAdapter.update(list);
                    } else if (response.errorBody() != null) {
                        try {
                            BaseActivity.showErrorMsg(response.errorBody().string(), response.code());
                        } catch (Exception e) {

                        }
                    }

                }

                @Override
                public void onFailure(Call<DeviceListObject> call, Throwable t) {
                    BaseActivity.hideDialog();
                    BaseActivity.showToast(t.getMessage());
                }
            });

        }
    }
    @Override
    public void onResume() {
        super.onResume();

        ((MyRecyclerViewAdapter) myRecyclerViewAdapter).setOnItemClickListener(new MyClickListener() {
            @Override
            public void onItemClick(int position, View v) {

                //  Base_Activity.callActivity(ProductDetailsActivity.class, null);
            }
        });
    }






    public Context getApplicationContext(){
        return getActivity();
    }




    public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.DataObjectHolder> {

        private List<DeviceListObject.PlantBean.SavingInfoBean> mDataset;

        private MyClickListener myClickListener;

        public class DataObjectHolder extends RecyclerView.ViewHolder
                implements View
                .OnClickListener {



            @BindView(R.id.tvTotal)
            SnTextView tvTotal;

            @BindView(R.id.tvToday)
            SnTextView tvToday;

            @BindView(R.id.tvCO2)
            SnTextView tvCO2;

            public DataObjectHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                myClickListener.onItemClick(getPosition(), v);
            }
        }

        public void setOnItemClickListener(MyClickListener myClickListener) {
            this.myClickListener = myClickListener;
        }

        public DeviceListObject.PlantBean.SavingInfoBean getSelectedItem(int position) {
            return mDataset.get(position);
        }

        public MyRecyclerViewAdapter(ArrayList<DeviceListObject.PlantBean.SavingInfoBean> myDataset) {
            mDataset = myDataset;
        }

        public void update(List<DeviceListObject.PlantBean.SavingInfoBean> myDataset) {
            mDataset = myDataset;
            notifyDataSetChanged();
        }

        @Override
        public MyRecyclerViewAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_revenue_list_item, parent, false);


            MyRecyclerViewAdapter.DataObjectHolder dataObjectHolder = new MyRecyclerViewAdapter.DataObjectHolder(view);
            return dataObjectHolder;
        }

        @Override
        public void onBindViewHolder(MyRecyclerViewAdapter.DataObjectHolder holder, int position) {
            final DeviceListObject.PlantBean.SavingInfoBean object = mDataset.get(position);
            holder.tvTotal.setText(Math.round(object.getTotal_Money_Saving())+"");
            holder.tvToday.setText(Math.round(object.getToday_Money_Saving())+"");
            holder.tvCO2.setText(object.getToday_Co2_Saving()+"");

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

        }

        public void addItem(DeviceListObject.PlantBean.SavingInfoBean dataObj, int index) {
            mDataset.add(dataObj);
            notifyItemInserted(index);

        }

        public void deleteItem(int index) {
            mDataset.remove(index);
            notifyItemRemoved(index);
        }

        @Override
        public int getItemCount() {

            return mDataset.size();
        }
    }


    @Override
    public void onStart() {
        super.onStart();

    }

    @Override
    public void onPause() {

        super.onPause();
    }


}
