package com.vnerds.solarpanel.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;


import com.vnerds.custom.SnTextView;
import com.vnerds.solarpanel.R;
import com.vnerds.solarpanel.fragments.DeviceListFragment;
import com.vnerds.solarpanel.fragments.RevenueListFragment;
import com.vnerds.solarpanel.fragments.WorkingInfoFragment;
import com.vnerds.solarpanel.objects.LoginObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends BaseActivity {

    @BindView(R.id.imgRevenue)
    ImageView imgRevenue;

    @BindView(R.id.imgDeviceInfo)
    ImageView imgDeviceInfo;

    @BindView(R.id.imgWorkinginfo)
    ImageView imgWorkinginfo;


    @BindView(R.id.imgGraph)
    ImageView imgGraph;


    @BindView(R.id.tvRevenue)
    SnTextView tvRevenue;

    @BindView(R.id.tvDeviceInfo)
    SnTextView tvDeviceInfo;

    @BindView(R.id.tvWorkinginfo)
    SnTextView tvWorkinginfo;


    @BindView(R.id.tvGraph)
    SnTextView tvGraph;


    @OnClick(R.id.llDeviceInfo)
    public void onClickllDeviceInfo(View view) {
        setHomeTab(2);
    }

    @OnClick(R.id.llRevenue)
    public void onClickllRevenue(View view) {
        setHomeTab(3);
    }

    @OnClick(R.id.llWorkinginfo)
    public void onClicklllWorkinginfo(View view) {
        setHomeTab(1);
    }

    @OnClick(R.id.llGraph)
    public void onClickllGraph(View view) {
        setHomeTab(4);
    }

    ActionBar actionBar;
    LoginObject login_object;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        login_object = getUserObject();

        String title = getResources().getString(R.string.app_name);

        actionBar = getSupportActionBar();
        actionBar.show();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(title);




        setHomeTab(1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }

        return true;
    }



    public void changeFragment(int position) {
        selectedPosition = position;
        Fragment fragment = new DeviceListFragment();
        Bundle bundle = getIntent().getExtras();
        switch (position) {
            case 1:
                actionBar.setTitle(getResources().getString(R.string.working_info));
                fragment = new WorkingInfoFragment();

                break;
            case 2:
                actionBar.setTitle(getResources().getString(R.string.device_info));
                fragment = new DeviceListFragment();

                break;
            case 3:
                actionBar.setTitle(getResources().getString(R.string.revenue));
                fragment = new RevenueListFragment();

                break;
        }
        if (fragment != null) {
            fragment.setArguments(bundle);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_frame, fragment, fragment.getClass().getSimpleName()).commit();
        }

    }
    int selectedPosition = 0;
    public void setHomeTab(int position) {
        changeFragment(position);
        switch (position) {
            case 1:
                imgWorkinginfo.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.icon_enabled));
                imgDeviceInfo.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.icon_disabled));
                imgRevenue.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.icon_disabled));
                imgGraph.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.icon_disabled));
                tvWorkinginfo.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.icon_enabled));
                tvDeviceInfo.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.icon_disabled));
                tvRevenue.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.icon_disabled));
                tvGraph.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.icon_disabled));
                break;
            case 2:
                imgWorkinginfo.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.icon_disabled));
                imgDeviceInfo.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.icon_enabled));
                imgRevenue.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.icon_disabled));
                imgGraph.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.icon_disabled));
                tvWorkinginfo.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.icon_disabled));
                tvDeviceInfo.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.icon_enabled));
                tvRevenue.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.icon_disabled));
                tvGraph.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.icon_disabled));
                break;
            case 3:
                imgWorkinginfo.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.icon_disabled));
                imgDeviceInfo.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.icon_disabled));
                imgRevenue.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.icon_enabled));
                imgGraph.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.icon_enabled));
                tvWorkinginfo.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.icon_disabled));
                tvDeviceInfo.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.icon_disabled));
                tvRevenue.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.icon_enabled));
                tvGraph.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.icon_enabled));
                break;
  case 4:
                imgWorkinginfo.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.icon_disabled));
                imgDeviceInfo.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.icon_disabled));
                imgRevenue.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.icon_disabled));
                imgGraph.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.icon_enabled));
                tvWorkinginfo.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.icon_disabled));
                tvDeviceInfo.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.icon_disabled));
                tvRevenue.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.icon_disabled));
                tvGraph.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.icon_enabled));
                break;

        }
    }

    protected void onPause() {

        super.onPause();
    }


    protected void onResume() {

        super.onResume();
    }


}