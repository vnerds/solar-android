package com.vnerds.solarpanel.objects;

import com.google.gson.annotations.SerializedName;

/**
 * Created by parag on 8/17/2017.
 */

public class CountryCode {

    /**
     * as : AS45774 Chandra Net Pvt. Limited, India
     * city : Ahmedabad
     * country : India
     * countryCode : IN
     * isp : SPiDiGO
     * lat : 23.0333
     * lon : 72.6167
     * org : Chandra Net Pvt. Limited, India
     * query : 175.100.171.42
     * region : GJ
     * regionName : Gujarat
     * status : success
     * timezone : Asia/Kolkata
     * zip : 380061
     */

    @SerializedName("as")
    private String as;
    @SerializedName("city")
    private String city;
    @SerializedName("country")
    private String country;
    @SerializedName("countryCode")
    private String countryCode;
    @SerializedName("isp")
    private String isp;
    @SerializedName("lat")
    private double lat;
    @SerializedName("lon")
    private double lon;
    @SerializedName("org")
    private String org;
    @SerializedName("query")
    private String query;
    @SerializedName("region")
    private String region;
    @SerializedName("regionName")
    private String regionName;
    @SerializedName("status")
    private String status;
    @SerializedName("timezone")
    private String timezone;
    @SerializedName("zip")
    private String zip;

    public String getAs() {
        return as;
    }

    public void setAs(String as) {
        this.as = as;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getIsp() {
        return isp;
    }

    public void setIsp(String isp) {
        this.isp = isp;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }
}
