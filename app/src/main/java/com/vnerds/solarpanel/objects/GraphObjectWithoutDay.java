package com.vnerds.solarpanel.objects;

import java.util.List;

public class GraphObjectWithoutDay extends CommonResponseObject {

    private GraphDataBean graph_data;

    public GraphDataBean getGraph_data() {
        return graph_data;
    }

    public void setGraph_data(GraphDataBean graph_data) {
        this.graph_data = graph_data;
    }

    public static class GraphDataBean {
        private SeriesBean series;
        private List<String> InverterID;
        private List<String> xAxis;

        public SeriesBean getSeries() {
            return series;
        }

        public void setSeries(SeriesBean series) {
            this.series = series;
        }

        public List<String> getInverterID() {
            return InverterID;
        }

        public void setInverterID(List<String> InverterID) {
            this.InverterID = InverterID;
        }

        public List<String> getXAxis() {
            return xAxis;
        }

        public void setXAxis(List<String> xAxis) {
            this.xAxis = xAxis;
        }

        public static class SeriesBean {
            private List<Double> F01175008453;

            public List<Double> getF01175008453() {
                return F01175008453;
            }

            public void setF01175008453(List<Double> F01175008453) {
                this.F01175008453 = F01175008453;
            }
        }
    }
}
