package com.vnerds.solarpanel.objects;

public class WorkingInfoObject extends CommonResponseObject {

    private PlantBean plant;

    public PlantBean getPlant() {
        return plant;
    }

    public void setPlant(PlantBean plant) {
        this.plant = plant;
    }

    public static class PlantBean {
        private String name;
        private String capacity;
        private WorkingInfoBean working_info;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCapacity() {
            return capacity;
        }

        public void setCapacity(String capacity) {
            this.capacity = capacity;
        }

        public WorkingInfoBean getWorking_info() {
            return working_info;
        }

        public void setWorking_info(WorkingInfoBean working_info) {
            this.working_info = working_info;
        }

        public static class WorkingInfoBean {
            private String total_energy;
            private String power_now;
            private String Today_Energy;
            private String yesterday_energy;
            private String Monthly_Energy;
            private String Yearly_Energy;

            public String getTotal_energy() {
                return total_energy;
            }

            public void setTotal_energy(String total_energy) {
                this.total_energy = total_energy;
            }

            public String getPower_now() {
                return power_now;
            }

            public void setPower_now(String power_now) {
                this.power_now = power_now;
            }

            public String getToday_Energy() {
                return Today_Energy;
            }

            public void setToday_Energy(String Today_Energy) {
                this.Today_Energy = Today_Energy;
            }

            public String getYesterday_energy() {
                return yesterday_energy;
            }

            public void setYesterday_energy(String yesterday_energy) {
                this.yesterday_energy = yesterday_energy;
            }

            public String getMonthly_Energy() {
                return Monthly_Energy;
            }

            public void setMonthly_Energy(String Monthly_Energy) {
                this.Monthly_Energy = Monthly_Energy;
            }

            public String getYearly_Energy() {
                return Yearly_Energy;
            }

            public void setYearly_Energy(String Yearly_Energy) {
                this.Yearly_Energy = Yearly_Energy;
            }
        }
    }
}
