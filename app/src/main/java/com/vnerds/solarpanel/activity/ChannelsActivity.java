package com.vnerds.solarpanel.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.facebook.login.LoginManager;
import com.github.captain_miao.recyclerviewutils.BaseWrapperRecyclerAdapter;
import com.github.captain_miao.recyclerviewutils.common.BaseLoadMoreFooterView;
import com.github.captain_miao.recyclerviewutils.listener.LinearLayoutWithRecyclerOnScrollListener;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.vnerds.custom.SharedPrefrenceUtil;
import com.vnerds.custom.SnTextView;
import com.vnerds.solarpanel.R;
import com.vnerds.solarpanel.application.SolarPanelApplication;
import com.vnerds.solarpanel.objects.ChannelListObject;
import com.vnerds.library.Constant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;
import in.srain.cube.views.ptr.header.MaterialHeader;
import in.srain.cube.views.ptr.util.PtrLocalDisplay;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChannelsActivity extends BaseActivity {

   @BindView(R.id.material_style_ptr_frame)
   PtrFrameLayout mPtrFrameLayout;

    private LinearLayoutWithRecyclerOnScrollListener mLoadMoreListener;

    SnTextView tvTitle;
    ImageView imgFav,imgSort,imgLogout;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.rvHome)
    RecyclerView rvHome;

    MyRecyclerViewAdapter myRecyclerViewAdapter;

    int pageCount = 0;
    private GoogleApiClient mGoogleApiClient;
    int selectedFilter = -1;
    LinearLayoutManager llm;
    FirebaseAuth mAuth ;
    FirebaseDatabase database;
    HashMap<Integer,ChannelListObject.ChannelsBean> channelsBeanHashMap;


    boolean isProfileSee = true;
    String favouriteText = "Add a favourite channel\nWhen it comes to channel,it's okay to " +
            "view favourites.Tap the star in the right side of any channel item.We'll save your favourites here for you.";
    String sortText = "Sort a channels by name or number \nTap here and select your sort order as you want.We'll save your sort here for you.";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channels);

        ButterKnife.bind(this);
        channelsBeanHashMap = new HashMap<>();
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        retriveFilterType();
        tvTitle =(SnTextView) mToolbar.findViewById(R.id.tvTitle);
        imgFav =(ImageView)mToolbar.findViewById(R.id.imgFav);
        imgLogout =(ImageView)mToolbar.findViewById(R.id.imgLogout);
        imgSort =(ImageView)mToolbar.findViewById(R.id.imgSort);
        imgSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectFilter();
            }
        });
        imgFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ChannelsActivity.this, FavoritesActivity.class);
                startActivityForResult(i, 1);

            }
        });
        imgLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ChannelsActivity.this);
                alertDialogBuilder.setTitle(mActivityBase.getString(R.string.app_name));
                alertDialogBuilder
                        .setMessage(R.string.wouldyouliketologout)
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mAuth.signOut();
                                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                                LoginManager.getInstance().logOut();
                                SharedPrefrenceUtil.setString(getApplicationContext(),getResources().getString(R.string.app_name), getString(R.string.token),null);
                                Constant.callActivity(ChannelsActivity.this,LoginActivity.class,null,true);
                            }
                        })
                        .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        })
                ;
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });
        GoogleSignInOptions options = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                // .requestIdToken(getString(R.string.webclientid))
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(ChannelsActivity.this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                } /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, options)
                .build();

        setSupportActionBar(mToolbar);
        tvTitle.setText("Channels");
        List<ChannelListObject.ChannelsBean> channelsBeanList = new ArrayList<>();
        myRecyclerViewAdapter = new MyRecyclerViewAdapter(channelsBeanList);

        rvHome.setAdapter(myRecyclerViewAdapter);
        rvHome.setNestedScrollingEnabled(false);
        llm = new LinearLayoutManager(this);
        rvHome.setLayoutManager(llm);



        myRecyclerViewAdapter.setLoadMoreFooterView(new BaseLoadMoreFooterView(this) {
            @Override
            public int getLoadMoreLayoutResource() {
                return R.layout.list_load_more;
            }
        });
        mLoadMoreListener = new LinearLayoutWithRecyclerOnScrollListener(llm) {
            @Override
            public void onLoadMore(final int pagination, final int pageSize) {
                rvHome.post(new Runnable() {
                    @Override
                    public void run() {
                        myRecyclerViewAdapter.showLoadMoreView();
                    }
                });
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadComplete();
                    }
                },2000);
            }
        };

        final MaterialHeader header = new MaterialHeader(this);

        int[] colors = getResources().getIntArray(R.array.google_colors);
        header.setColorSchemeColors(colors);
        header.setLayoutParams(new PtrFrameLayout.LayoutParams(-1, -2));
        header.setPadding(0, PtrLocalDisplay.dp2px(15), 0, PtrLocalDisplay.dp2px(10));
        header.setPtrFrameLayout(mPtrFrameLayout);


        mPtrFrameLayout.setHeaderView(header);
        mPtrFrameLayout.addPtrUIHandler(header);

        mPtrFrameLayout.setEnabledNextPtrAtOnce(true);
        mPtrFrameLayout.setPtrHandler(new PtrHandler() {
            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                    return mLoadMoreListener.checkCanDoRefresh();
            }
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                mPtrFrameLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        callChannelAPI();
                    }
                }, 500);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mLoadMoreListener.loadComplete();
                    }
                },2000);
            }
        });

        rvHome.addOnScrollListener(mLoadMoreListener);
        retriveFavoriteList();


    }




    public void callChannelAPI() {
        if (isNetworkAvailable()) {

            Call<ChannelListObject> call = null;//SolarPanelApplication.getApiServices().getChannelList();
            call.enqueue(new Callback<ChannelListObject>() {
                @Override
                public void onResponse(Call<ChannelListObject> call, Response<ChannelListObject> response) {
                    mPtrFrameLayout.refreshComplete();
                    final ChannelListObject channelListObject = response.body();
                    if (channelListObject != null) {

                        mLoadMoreListener.loadComplete();
                    }else if (response.errorBody() != null) {
                        try {
                            showErrorMsg(response.errorBody().string(),response.code());
                        } catch (Exception e) {

                        }
                    }

                }



                @Override
                public void onFailure(Call<ChannelListObject> call, Throwable t) {
                    hideDialog();
                    showToast(t.getMessage());
                }
            });

        }
    }
    public class MyRecyclerViewAdapter extends BaseWrapperRecyclerAdapter<ChannelListObject.ChannelsBean,MyRecyclerViewAdapter.DataObjectHolder> {


        public class DataObjectHolder extends RecyclerView.ViewHolder
                {
            @BindView(R.id.tvName)
            SnTextView tvName;

            @BindView(R.id.imgFavorite)
            ImageView imgFavorite;

            public DataObjectHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }

        public MyRecyclerViewAdapter(List<ChannelListObject.ChannelsBean> myDataset) {
           appendToList(myDataset);
        }

        @Override
        public DataObjectHolder onCreateItemViewHolder(ViewGroup parent,
                                                   int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.activity_channel_list_item, parent, false);

            DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
            return dataObjectHolder;
        }

        @Override
        public void onBindItemViewHolder(final DataObjectHolder holder, int position) {
            final ChannelListObject.ChannelsBean channelsBean = getItem(position);
            holder.tvName.setText(channelsBean.getChannelId()+" "+channelsBean.getChannelTitle());
            if(channelsBeanHashMap.containsKey(channelsBean.getChannelId())){
                holder.imgFavorite.setColorFilter(ContextCompat.getColor(holder.tvName.getContext(), R.color.colorAccent));
            }else{
                holder.imgFavorite.setColorFilter(ContextCompat.getColor(holder.tvName.getContext(), R.color.textHintColor));
            }
            holder.imgFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(channelsBeanHashMap.containsKey(channelsBean.getChannelId())){
                      removeFromFavorite(channelsBean);
                    }else{
                        addToFavorite(channelsBean);
                    }

                }
            });
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   // addToFavorite(channelsBean);
                }
            });
        }
    }
    public void selectFilter() {
        int selectedPosition = -1;

        final String[] items = getResources().getStringArray(R.array.filter);
        if (selectedFilter > -1) {
            for (int i = 0; i < items.length; i++) {
                if (selectedFilter == i) {
                    selectedPosition = i;
                    break;
                }
            }
        }
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Filter");
        builder.setSingleChoiceItems(items, selectedPosition, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                selectedFilter = item;
            }
        });
        builder.setPositiveButton(getString(R.string.apply), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                performOperation();
                saveFilterType();
                dialog.dismiss();
            }
        }).setNeutralButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                selectedFilter = -1;
                dialog.dismiss();
            }
        })
                .setNegativeButton(getString(R.string.clear), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        selectedFilter = -1;
                        removeFilterType();
                        dialog.dismiss();
                    }
                });
        AlertDialog levelDialog = builder.create();
        levelDialog.show();
    }
    public void performOperation(){
        switch (selectedFilter)
        {
            case 0:
                ascendingOrderByName();
                break;
            case 1:
                descendingOrderByName();
                break;
            case 2:
                ascendingOrderByNumber();
                break;
            case 3:
                descendingOrderByNumber();
                break;
        }
        rvHome.scrollToPosition(0);
    }
    public void retriveFavoriteList(){
        showDialog();
        channelsBeanHashMap.clear();
        String userId = mAuth.getCurrentUser().getUid();
        DatabaseReference myRef = database.getReference("favourite").child(userId).child("list");

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hideDialog();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    ChannelListObject.ChannelsBean channelsBean = postSnapshot.getValue(ChannelListObject.ChannelsBean.class);
                    channelsBeanHashMap.put(channelsBean.getChannelId(),channelsBean);
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mPtrFrameLayout.autoRefresh(true,100);
                    }
                },100);

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                hideDialog();
                showToast("Oops " + databaseError.getMessage());
            }
        });
    }
    public void retriveFilterType(){
        showDialog();
        channelsBeanHashMap.clear();
        String userId = "";
        if(mAuth!=null){
            userId = mAuth.getCurrentUser().getUid();
        }

        DatabaseReference myRef = database.getReference("favourite").child(userId).child("filter");

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hideDialog();
                selectedFilter = dataSnapshot.getValue()!=null?dataSnapshot.getValue(Integer.class):-1;
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                hideDialog();
                showToast("Oops " + databaseError.getMessage());
            }
        });
    }
    public void addToFavorite(final ChannelListObject.ChannelsBean channelsBean){
        DatabaseReference myRef = database.getReference("favourite");
        String userId = mAuth.getCurrentUser().getUid();
        myRef.child(userId).child("list").child(myRef.push().getKey()).setValue(channelsBean);

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                showToast("Favourite added");
                channelsBeanHashMap.put(channelsBean.getChannelId(),channelsBean);
                myRecyclerViewAdapter.notifyDataSetChanged();
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                showToast("Oops " + databaseError.getMessage());
            }
        });
    }
    public void saveFilterType(){
        DatabaseReference myRef = database.getReference("favourite");
        String userId = mAuth.getCurrentUser().getUid();
        myRef.child(userId).child("filter").setValue(selectedFilter);

        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                showToast("Filter saved");

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                showToast("Oops " + databaseError.getMessage());
            }
        });
    }
    public void removeFilterType(){
        DatabaseReference myRef = database.getReference("favourite");
        String userId = mAuth.getCurrentUser().getUid();
        myRef.child(userId).child("filter").removeValue();
        showToast("Filter removed");
        retriveFavoriteList();
    }
    public void removeFromFavorite(final ChannelListObject.ChannelsBean channelsBean){
        DatabaseReference myRef = database.getReference("favourite");
        String userId = mAuth.getCurrentUser().getUid();
        Query deleteQuery = myRef.child(userId).child("list").orderByChild("channelId").equalTo(channelsBean.getChannelId());
        deleteQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                dataSnapshot.getRef().removeValue();
                showToast("Favourite removed");
                channelsBeanHashMap.remove(channelsBean.getChannelId());
                myRecyclerViewAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    //For sorting
    public void ascendingOrderByName(){
        Collections.sort(myRecyclerViewAdapter.getList(), new Comparator<ChannelListObject.ChannelsBean>() {
            @Override
            public int compare(ChannelListObject.ChannelsBean lhs, ChannelListObject.ChannelsBean rhs) {
                return lhs.getChannelTitle().compareTo(rhs.getChannelTitle());
            }
        });

        myRecyclerViewAdapter.notifyDataSetChanged();
    }
    public void descendingOrderByName(){
        Collections.sort(myRecyclerViewAdapter.getList(), new Comparator<ChannelListObject.ChannelsBean>() {
            @Override
            public int compare(ChannelListObject.ChannelsBean lhs, ChannelListObject.ChannelsBean rhs) {
                return rhs.getChannelTitle().compareTo(lhs.getChannelTitle());
            }
        });
        myRecyclerViewAdapter.notifyDataSetChanged();
    }
    public void ascendingOrderByNumber(){
        Collections.sort(myRecyclerViewAdapter.getList(), new Comparator<ChannelListObject.ChannelsBean>() {
            @Override
            public int compare(ChannelListObject.ChannelsBean lhs, ChannelListObject.ChannelsBean rhs) {
                return lhs.getChannelId() > rhs.getChannelId() ? 1 :(lhs.getChannelId() < rhs.getChannelId() ? -1 : 0);
            }
        });

        myRecyclerViewAdapter.notifyDataSetChanged();
    }
    public void descendingOrderByNumber(){
        Collections.sort(myRecyclerViewAdapter.getList(), new Comparator<ChannelListObject.ChannelsBean>() {
            @Override
            public int compare(ChannelListObject.ChannelsBean lhs, ChannelListObject.ChannelsBean rhs) {
                return lhs.getChannelId() > rhs.getChannelId() ? -1 :(lhs.getChannelId() < rhs.getChannelId() ? 1 : 0);
            }
        });
        myRecyclerViewAdapter.notifyDataSetChanged();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                boolean valueUpdated = data.getBooleanExtra("update",false);
                if(valueUpdated){
                    retriveFavoriteList();
                }
            }
        }
    }
}
