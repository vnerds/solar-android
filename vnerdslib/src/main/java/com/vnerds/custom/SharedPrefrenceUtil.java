package com.vnerds.custom;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.vnerds.library.R;

/**
 * This class is used for storing and retrieving shared preference values.
 * Parag Chauhan
 */
public class SharedPrefrenceUtil {



	public static SharedPreferences getPrefs(Context context,String appName) {
		return context.getSharedPreferences(appName, context.MODE_PRIVATE);
	}


	public static String getString(Context context,String appName, String key) {

		return getPrefs(context,appName).getString((key), "");
	}
	public static void setString(Context context,String appName, String key, String value) {

		getPrefs(context, appName).edit().putString(key,(value)).commit();
	}



	public static void setInt(Context context,String appName, String key, int value) {
		getPrefs(context,appName).edit().putInt(key, value).commit();
	}
	public static int getInt(Context context,String appName, String key, int defaultValue) {
		return getPrefs(context,appName).getInt(key, defaultValue);
	}




	public static void setBoolean(Context context,String appName, String key, boolean value) {
		getPrefs(context,appName).edit().putBoolean(key,value).commit();
	}
	public static boolean getBoolean(Context context,String appName, String key) {
		return getPrefs(context,appName).getBoolean(key, false);
	}

	public static void setCustomObject(Context context,String appName, String key, Object object) {

		SharedPreferences.Editor prefsEditor = getPrefs(context,appName).edit();
		Gson gson = new Gson();
		String json = gson.toJson(object);
		prefsEditor.putString(key, (json));
		prefsEditor.commit();
	}
	public static Object getCustomObject(Context context,String appName, String key, Object object) {

		Gson gson = new Gson();
		String json =  getPrefs(context,appName).getString(key, "");
		Object obj = gson.fromJson((json), object.getClass());
		return obj;
	}
}