package com.vnerds.solarpanel.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.vnerds.custom.SnButton;
import com.vnerds.custom.SnEditText;
import com.vnerds.library.Constant;
import com.vnerds.solarpanel.R;
import com.vnerds.solarpanel.apis.APIClass;
import com.vnerds.solarpanel.application.SolarPanelApplication;
import com.vnerds.solarpanel.objects.Global;
import com.vnerds.solarpanel.objects.LoginObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddDepartmentActivity extends BaseActivity {



    @BindView(R.id.edtName)
    SnEditText edtName;

    @BindView(R.id.edtCity)
    SnEditText edtCity;

    @BindView(R.id.edtAddress)
    SnEditText edtAddress;

    @BindView(R.id.edtUserName)
    SnEditText edtUserName;

    @BindView(R.id.edtPassword)
    SnEditText edtPassword;

    @BindView(R.id.btnAdd)
    SnButton btnAdd;

    LoginObject userObject ;
    Activity mActivity ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adddepartment);
        ButterKnife.bind(this);
        String title = getResources().getString(R.string.adddepartment);
        mActivity = this;

        if (Global.departmentBean != null) {
            title = getString(R.string.editdepartment);
            edtName.setText(Global.departmentBean.getName());
            edtAddress.setText(Global.departmentBean.getAddress());
            edtCity.setText(Global.departmentBean.getCity());
            edtUserName.setText(Global.departmentBean.getUname());
            //edtPassword.setText(Global.departmentBean.get());
            btnAdd.setText(getResources().getString(R.string.update));
        }


        ActionBar actionBar = getSupportActionBar();
        actionBar.show();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(title);
        userObject = getUserObject();
      /*  edtUserName.setText(userObject.getUser().getUname());
        edtUserName.setEnabled(false);
        edtUserName.setClickable(false);*/


    }







    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    @OnClick(R.id.btnAdd)
    public void onCLickLogin(View view) {
        if (isEditTextValid(edtName, getString(R.string.validation_name))) {
            if (isEditTextValid(edtAddress, getString(R.string.validation_address))) {
                if (isEditTextValid(edtCity, getString(R.string.validation_city))) {
                    if (isEditTextValid(edtUserName, getString(R.string.validation_username))) {
                        if (isEditTextValid(edtPassword, getString(R.string.validation_password))) {
                            callAddOEMDepartmentAPI();
                        }
                    }
                }
            }
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Global.departmentBean = null;
    }


    public void callAddOEMDepartmentAPI() {
        if (isNetworkAvailable()) {
            showDialog();
//name, oem_id, address, state_id, uname and password, token
            Call<LoginObject> call = SolarPanelApplication.getApiServices().addDepartment(
                    getEditText(edtName),Global.oemBean.getOem_id(),
                    getEditText(edtAddress),Global.stateBean.getState_id(),
                    getEditText(edtUserName),
                    getEditText(edtPassword),
                    userObject.getToken(),
                    APIClass.ADD,
            getEditText(edtCity));
            if(Global.departmentBean!=null){
                call = SolarPanelApplication.getApiServices().editDepartment(
                        Global.departmentBean.getDepartment_id(),
                        getEditText(edtName),Global.oemBean.getOem_id(),
                        getEditText(edtAddress),Global.stateBean.getState_id(),
                        getEditText(edtUserName),
                        getEditText(edtPassword),
                        userObject.getToken(),
                        APIClass.EDIT,
                        getEditText(edtCity));
            }
            call.enqueue(new Callback<LoginObject>() {
                @Override
                public void onResponse(Call<LoginObject> call, Response<LoginObject> response) {
                    hideDialog();
                    LoginObject object = response.body();
                    if (object != null && object.getError()==0 && isValidResponse(object)) {
                        Constant.isOEMAdded = true;
                        showToastWithExit(object.getSuccess_msg());
                    }else if(response.errorBody()!=null){
                        try {
                            showErrorMsg(response.errorBody().string(),response.code());
                        }catch (Exception e){

                        }
                    }else if (object != null && object.getError()==1){
                        showToast(object.getError_msg());
                    }
                }

                @Override
                public void onFailure(Call<LoginObject> call, Throwable t) {
                   hideDialog();
                    showToast(t.getMessage());
                    //listView.onRefreshComplete();
                    Log.d("CallBack", " Throwable is " + t);
                }
            });


        }
    }


    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();

    }


}