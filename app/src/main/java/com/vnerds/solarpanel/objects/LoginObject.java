package com.vnerds.solarpanel.objects;

/**
 * Created by home on 2/13/2018.
 */

public class LoginObject extends CommonResponseObject{


    /**
     * user_id : 7
     * status : 1
     * user : {"uname":"krunal","mainadmin":1}
     * token : eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjciLCJlbWFpbCI6ImtydW5hbCIsImV4cCI6MTUxODQ5Nzg0Nn0.ZyC9cXQyhLQM_z_L_7t550FUOQ1LylSRDlYV5mFSjr8
     * access : {"header":{"report_info":"1","history_data":"1"},"plant":{"graph":"1"},"device":{"graph_first":"1","graph_second":"1"},"invt":{"graph":"1","table":"1"}}
     */

    private int user_id;
    private int status;
    /**
     * uname : krunal
     * mainadmin : 1
     */

    private UserBean user;
    private String token;
    /**
     * header : {"report_info":"1","history_data":"1"}
     * plant : {"graph":"1"}
     * device : {"graph_first":"1","graph_second":"1"}
     * invt : {"graph":"1","table":"1"}
     */

    private AccessBean access;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public AccessBean getAccess() {
        return access;
    }

    public void setAccess(AccessBean access) {
        this.access = access;
    }

    public static class UserBean {
        private String uname;
        private int mainadmin;

        public String getUname() {
            return uname;
        }

        public void setUname(String uname) {
            this.uname = uname;
        }

        public int getMainadmin() {
            return mainadmin;
        }

        public void setMainadmin(int mainadmin) {
            this.mainadmin = mainadmin;
        }
    }

    public static class AccessBean {
        /**
         * report_info : 1
         * history_data : 1
         */

        private HeaderBean header;
        /**
         * graph : 1
         */

        private PlantBean plant;
        /**
         * graph_first : 1
         * graph_second : 1
         */

        private DeviceBean device;
        /**
         * graph : 1
         * table : 1
         */

        private InvtBean invt;

        public HeaderBean getHeader() {
            return header;
        }

        public void setHeader(HeaderBean header) {
            this.header = header;
        }

        public PlantBean getPlant() {
            return plant;
        }

        public void setPlant(PlantBean plant) {
            this.plant = plant;
        }

        public DeviceBean getDevice() {
            return device;
        }

        public void setDevice(DeviceBean device) {
            this.device = device;
        }

        public InvtBean getInvt() {
            return invt;
        }

        public void setInvt(InvtBean invt) {
            this.invt = invt;
        }

        public static class HeaderBean {
            private String report_info;
            private String history_data;

            public String getReport_info() {
                return report_info;
            }

            public void setReport_info(String report_info) {
                this.report_info = report_info;
            }

            public String getHistory_data() {
                return history_data;
            }

            public void setHistory_data(String history_data) {
                this.history_data = history_data;
            }
        }

        public static class PlantBean {
            private String graph;

            public String getGraph() {
                return graph;
            }

            public void setGraph(String graph) {
                this.graph = graph;
            }
        }

        public static class DeviceBean {
            private String graph_first;
            private String graph_second;

            public String getGraph_first() {
                return graph_first;
            }

            public void setGraph_first(String graph_first) {
                this.graph_first = graph_first;
            }

            public String getGraph_second() {
                return graph_second;
            }

            public void setGraph_second(String graph_second) {
                this.graph_second = graph_second;
            }
        }

        public static class InvtBean {
            private String graph;
            private String table;

            public String getGraph() {
                return graph;
            }

            public void setGraph(String graph) {
                this.graph = graph;
            }

            public String getTable() {
                return table;
            }

            public void setTable(String table) {
                this.table = table;
            }
        }
    }
}
