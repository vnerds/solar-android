package com.vnerds.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.vnerds.library.R;


/**
 * Created by parag.chauhan on 6/18/2015.
 */
public class SnTextView extends android.support.v7.widget.AppCompatTextView {
    public String fontName ;
    public SnTextView(Context context) {
        super(context);
     //  init();
    }

    public SnTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs,0);
    }

    public SnTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs,defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SnTextView, defStyle, 0);
        fontName = a.getString(R.styleable.SnTextView_fonttv);
        a.recycle();
        String text = getText().toString();
        System.out.print(text);
        if(fontName != null) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), fontName);
            setTypeface(tf);
        }else{
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), getResources().getString(R.string.medium));
            setTypeface(tf);
        }

    }
}
